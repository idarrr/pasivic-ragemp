CREATE TABLE IF NOT EXISTS `_commands` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`command` VARCHAR(255),
	`description` TEXT,
	`syntax` TEXT,
    `admin` INT(2) NOT NULL DEFAULT 0,
    `supporter` INT(2) NOT NULL DEFAULT 0,
    `executive` INT(2) NOT NULL DEFAULT 0,
    `player` INT(2) NOT NULL DEFAULT 0,
    `faction` INT(2) NOT NULL DEFAULT 0,
    `factionType` INT(2) NOT NULL DEFAULT 0
)

CREATE TABLE IF NOT EXISTS `_binds` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `account` INT,
    `key` INT,
    `command` VARCHAR(255),
    `args` TEXT
)

CREATE TABLE IF NOT EXISTS `account` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`username` VARCHAR,
    `salt` VARCHAR,
    `password` TEXT,
    `ip` TEXT,
    `serial` TEXT,
    `admin` INT NOT NULL DEFAULT 0,
    `supporter` INT NOT NULL DEFAULT 0,
    `executive` INT NOT NULL DEFAULT 0,
    `hiddenadmin` INT NOT NULL DEFAULT 0,
    `registerdate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `lastlogin` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `app` INT NOT NULL DEFAULT 0,
    `questions` JSON,
    `answers` JSON,
    `acceptedby` VARCHAR,
    `wallet` INT NOT NULL DEFAULT 0,
)

CREATE TABLE IF NOT EXISTS `characters` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR,
    `skin` JSON,
    `accessories` JSON,
    `birthday` TIMESTAMP,
    `gender` INT,
    `health` INT NOT NULL DEFAULT 100,
    `armor` INT NOT NULL DEFAULT 100,
    `hunger` INT NOT NULL DEFAULT 100,
    `thirst` INT NOT NULL DEFAULT 100,
    `mood` INT NOT NULL DEFAULT 100,
    `faction` INT NOT NULL DEFAULT 0,
    `mask` INT NOT NULL DEFAULT 0,
)

CREATE TABLE IF NOT EXISTS `bans` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `account` INT,
    `ip` TEXT,
    `serial` TEXT,
    `bannedby` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `duration` INT,
)

CREATE TABLE IF NOT EXISTS `ban_history` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `account` INT,
    `ip` TEXT,
    `serial` TEXT,
    `bannedby` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `duration` INT,
)

CREATE TABLE IF NOT EXISTS `jail` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `account` INT,
    `character` INT,
    `jailedby` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `duration` INT,
)

CREATE TABLE IF NOT EXISTS `jail_history` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `account` INT,
    `character` INT,
    `jailedby` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `duration` INT,
)

CREATE TABLE IF NOT EXISTS `character_name_history` (
    `account` INT,
    `from_name` VARCHAR,
    `to_name` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
)

CREATE TABLE IF NOT EXISTS `warns` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `character` INT,
    `description` TEXT,
    `warnedby` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
)

CREATE TABLE IF NOT EXISTS `warn_history` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `character` INT,
    `description` TEXT,
    `warnedby` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
)

CREATE TABLE IF NOT EXISTS `mask_history` (
    `maskid` INT,
    `character` VARCHAR,
    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
)