﻿using RAGE;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class CameraControl
    {
        private static int CurrentCam = 0;
        public static string FLY_CAM = "DEFAULT_SCRIPTED_FLY_CAMERA";
        public static string DEFAULT_CAM = "DEFAULT_SCRIPTED_CAMERA";
        public static string ANIMATED_CAM = "DEFAULT_ANIMATED_CAMERA";
        public static string SPLINE_CAM = "DEFAULT_SPLINE_CAMERA";

        public static void SetCameraPosition(Vector3 position, string CameraType)
        {
            SetCameraToDefault();
            RAGE.Game.Cam.CreateCamWithParams(CameraType, position.X, position.Y, position.Z, 0, 0, 0, 40, true, 2);
        }

        public static void SetCameraRotation(Vector3 position)
        {
            RAGE.Game.Cam.SetCamRot(CurrentCam, position.X, position.Y, position.Z, 2);
        }

        public static void SetCameraPointAt(Vector3 position)
        {
            RAGE.Game.Cam.PointCamAtCoord(CurrentCam, position.X, position.Y, position.Z);
        }

        public static void SetCameraToDefault()
        {
            RAGE.Game.Cam.RenderScriptCams(false, false, 0, true, false, CurrentCam);
            RAGE.Game.Cam.DestroyAllCams(true);
            CurrentCam = 0;
        }

        public static int GetCurrentCamera()
        {
            return CurrentCam;
        }
    }
}
