﻿using RAGE;
using RAGE.Game;
using RAGE.NUI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class Controller : Events.Script
    {
        private static List<int> DisabledMoveControl = new List<int>();
        public static bool TemporaryDisableControl = false;

        public Controller ()
        {
            Events.Tick += TickEvent;
        }

        private void TickEvent(List<Events.TickNametagData> nametags)
        {
            if (TemporaryDisableControl)
            {
                for (int i = 0; i < 357; i++)
                {
                    Pad.DisableControlAction(0, i, true);                    
                }
            } else
            {
                foreach (var item in DisabledMoveControl)
                {
                    Pad.DisableControlAction(0, item, true);
                }
            }
        }

        public static void DisableMoveControl(int action)
        {
            DisabledMoveControl.Add(action);
        }

        public static void EnableMoveControl(int action)
        {
            
            DisabledMoveControl.Remove(action);
        }
    }
}
