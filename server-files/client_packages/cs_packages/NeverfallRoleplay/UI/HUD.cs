﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RAGE;
using RAGE.Elements;
using RAGE.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class HUD : Events.Script
    {
        private static int sW;
        private static int sH;
        public static int ScreenWidth { get => sW; }
        public static int ScreenHeight { get => sH; }
        
        public HUD ()
        {
            int w = 0;
            int h = 0;
            RAGE.Game.Graphics.GetActiveScreenResolution(ref w, ref h);
            sW = h;
            sH = w;
            Key.Bind(Key.KEY_M, true, ToggleCursor);
            UpdateNametag();
        }

        public void ToggleCursor ()
        {
            if (CefUI.IsInputActive())
            {
                return;
            }
            if (!Chatbox.Active)
            {
                Cursor.Visible = !Cursor.Visible;
            }
        }

        public void UpdateNametag()
        {
            var players = Entities.Players.All;
            Chatbox.Send($"Count {players.Count}");
            JObject obj = new JObject();
            foreach (var player in players)
            {
                float x = 0;
                float y = 0;
                var pos = player.GetWorldPositionOfBone(12844);
                RAGE.Game.Graphics.GetScreenCoordFromWorldCoord(pos.X, pos.Y, pos.Z, ref x, ref y);

                //obj[player.Name] = new JObject(new float[] { x, y });
            }
            var str = JsonConvert.SerializeObject(obj);
            Chatbox.Send(str);
        }
    }
}
