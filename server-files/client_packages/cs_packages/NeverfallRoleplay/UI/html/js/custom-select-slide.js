﻿var event = new CustomEvent("onCustomSelectChange");
function customSelectSlideInit(element) {
    let display = null;
    let p = document.getElementById(element.getAttribute("items"));
    if (p == null) { return console.log("Custom select not initialized on ", element);}
    var childs = p.children;
    
    for (let i = 0; i < element.children.length; i++) {
        if (element.children[i].getAttribute("as") == "display") {
            display = element.children[i];
        }
    }
    let selected = 0;
    if (childs.length > 0) {
        var text = childs[0].innerHTML;
        for (let i = 0; i < childs.length; i++) {
            if (childs[i].getAttribute("selected") == "true") {
                selected = i;
                text = childs[i].innerHTML;
                break;
            }
        }
        if (selected == 0) {
            childs[selected].setAttribute("selected", "true");
        }
        if (display != null) {
            display.setAttribute("selected", selected);
            display.innerHTML = text;
        }
    }
}

function customSelectSetSelected(element, index, dontDispatch) {
    let display = null;
    let p = document.getElementById(element.getAttribute("items"));
    if (p == null) { return console.log("Custom select not found on ", element); }
    let childs = p.children;

    for (var i = 0; i < element.children.length; i++) {
        if (element.children[i].getAttribute("as") == "display") {
            display = element.children[i];
        }
    }
    let selected = 0;
    if (childs.length > 0) {
        var text = childs[0].innerHTML;
        for (let i = 0; i < childs.length; i++) {
            if (childs[i].getAttribute("selected") == "true") {
                childs[i].removeAttribute("selected");
            }
        }
        if (childs[index]) {
            selected = index;
            text = childs[index].innerHTML;
            childs[selected].setAttribute("selected", "true");
            display.setAttribute("selected", selected);
            display.innerHTML = text;
            element.setAttribute("selected", index);
            p.setAttribute("selected", index);
            if (dontDispatch !== true) {
                element.dispatchEvent(event);
                p.dispatchEvent(event);
            }
        }
        
    }
}

$(document).ready(() => {
    var selects = document.querySelectorAll("div[customSelect='true']")
    
    for (var i = 0; i < selects.length; i++) {
        
        customSelectSlideInit(selects[i]);
    }

    $(".custom-select-slide-btn").on("click", (e) => {
        let parent = e.target.parentElement
        let itemsId = parent.getAttribute("items");
        let element = document.getElementById(itemsId);
        let dir = true;
        if (e.target.getAttribute("as") == "previous") {
            dir = false;
        }
        let childs = element.children;
        let display = null;
        for (var i = 0; i < parent.children.length; i++) {
            if (parent.children[i].getAttribute("as") == "display") {
                display = parent.children[i];
            }
        }

        for (var i = 0; i < childs.length; i++) {
            var c = childs[i];
            if (c.getAttribute("selected") == "true") {
                let index = 0;
                if (dir) {
                    if (childs[i + 1] != null) {
                        index = i + 1;
                    }
                } else {
                    if (i == 0) {
                        index = childs.length - 1;
                    } else if (childs[i - 1] != null) {
                        index = i - 1;
                    }
                }
                
                c.removeAttribute("selected");
                childs[index].setAttribute("selected", "true");
                display.innerHTML = childs[index].innerHTML;
                //display.setAttribute("selected", index);
                parent.setAttribute("selected", index);
                parent.dispatchEvent(event);
                element.setAttribute("selected", index);
                element.dispatchEvent(event);
                break;
            }
        }
    })
})
