﻿var inputstate;
$(document).on("focus click", (e) => {
    if (typeof mp === 'undefined') { return };
    var tag = $(e.target).prop("tagName");
    if (tag == "INPUT" || tag == "SELECT") {
        // focus
        if (inputstate == null || inputstate === false) {
            inputstate = true;
        }
    } else {
        // unfocus
        if (inputstate == null || inputstate === true) {
            inputstate = false;
        }
    }
    mp.trigger("OnInputFocusChange", inputstate);
});