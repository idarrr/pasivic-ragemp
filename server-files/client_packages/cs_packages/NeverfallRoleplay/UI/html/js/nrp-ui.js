var draggedWindow;
var drag = false;
var event;
var currentHover;
var mouseDown = false;
var hidden = false;
var nametags = [];
$(document).ready(function () {

});

function onMouseMove() {
    if (event) {
        var window = draggedWindow;
        e = event || window.event;
        e.preventDefault();
        var pos1 = window.data("pos3") - e.clientX;
        var pos2 = window.data("pos4") - e.clientY;
        var pos3 = e.clientX;
        var pos4 = e.clientY;

        window.data("pos1", pos1);
        window.data("pos2", pos2);
        window.data("pos3", pos3);
        window.data("pos4", pos4);
        window.offset({ top: (window.offset().top - pos2), left: (window.offset().left - pos1) });
    }
    requestAnimationFrame(onMouseMove);
};
requestAnimationFrame(onMouseMove);

$(document).mousemove(function (e) {
    if (draggedWindow && mouseDown) {
        //onMouseMove(e, draggedWindow);
        event = e;
        //drag = true;
    } else {
        if (e.target) {
            var el = $(e.target);
            if (el.hasClass('window-header')) {
                var window = el.parent(".window");
                window.data("pos1", 0);
                window.data("pos2", 0);
                window.data("pos3", e.clientX);
                window.data("pos4", e.clientY);
                draggedWindow = window;
            } else {
                draggedWindow = null;
            }
        }
        event = null;
    }
});

$(document).mousedown(function (e) {
    if (e.which === 1) {
        mouseDown = true;
        var el = $(e.target);
        if (el.parents(".window").length) {
            var window = el.parents(".window");
            window.siblings('.window').css('z-index', 10);
            window.css('z-index', 11);
        }
    }
});

$(document).mouseup(function (e) {
    if (e.which === 1) {
        mouseDown = false;
    }
});

function closeWindow(id) {
    var window = $(document.getElementById(id));
    console.log(window.length);
    if (window.length) {
        window.remove();
    }
};

function closeUI(id) {
    var window = $(document.getElementById("frame-"+id));
    console.log(window.length);
    if (window.length) {
        window.remove();
    }
};

function loadWindow(path, title, x, y, w, h, query) {
    var element = $("#" + path);
    if (element.length) {
        console.log(element);
        console.log("Element ID "+path+" exists!");
        return;
    }
    $('body').append(`
        <div id="`+ path + `" style="top: ` + x + `px; left: ` + y +`px;" class="card window text-white bg-dark border-dark fade-in">
            <div class= "card-header window-header ripple">
            <div class="card-label">
                <b>`+ title +`</b>
            </div>
        <div class="controller">
            <div class="float-right">
                <a href="#" class="btn btn-ctrl btn-danger ripple" onClick='closeWindow("`+ path +`");'>&times;</a>
            </div>
        </div>
        </div>
        <iframe id="frame-`+path+`" src="`+ path + `?` + query +`>" frameborder="0" width="`+w+`" height="`+h+`"></iframe>
    </div >`);
};

function loadUI(path, id, x, y, w, h, query) {
    var element = $("#frame-" + id);
    if (element.length) {
        console.log(element);
        console.log("Element ID " + id + " exists!");
        return;
    }
    $('body').append(`
        <iframe style="top: ` + x + `px; left: ` + y + `px;" id="frame-` + id + `" src="` + path + `?` + query + `>" frameborder="0" width="` + w + `" height="` + h + `"></iframe>
    `);
};

function execute(id, name, ...args) {
    console.log(id, name, ...args);
    document.getElementById('frame-' + id).contentWindow[name](...args);
};
