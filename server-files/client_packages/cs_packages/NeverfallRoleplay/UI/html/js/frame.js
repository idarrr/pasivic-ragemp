﻿/* Frame Connection */
/* This JavaScript Code belongs to Neverfall Roleplay */

$(document).ready(function () {
    mp.trigger("OnWindowLoaded", window.location.pathname.split('/').pop());
});

function callBack(eventName, ...args) {
    mp.trigger('CefCallback', eventName, ...args);
};

function getURLQueries() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
};