﻿var chatfocused = false;
var chatPrefix = "";
$(document).ready(function () {
    $('#chat-panel').on('change keyup keydown paste cut', 'textarea', function () {
        $(this).height(0).height(this.scrollHeight);
    }).find('textarea').change();
    $("#chat-input").on('keyup', function (e) {
        
        if (e.which == 13) {
            e.preventDefault();
            $(this).val("");
        }
    });
    $("#chat-input").on('keydown', function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var val = $(this).val();
            $(this).val("");
            if (val.startsWith("/") && chatPrefix == "/t ") {
                var sp = val.split(" ");
                chatPrefix = sp[0];
                val = "";
                for (i = 0; i < sp.length; i++) {
                    if (i != 0) {
                        val = val + " " + sp[i];
                    }
                }
            }
            //disableChatInput()
            mp.trigger("OnChatEnter", chatPrefix + val);
            chatPrefix = "";
        }
    });
    $("#chat-input").click(function (e) {
        e.preventDefault();
    });
});

/*
$(document).on("keyup", function (e) {
    if (e.which == 84) {
        console.log("pressed");
        if (!chatfocused) {
            enableChatInput();
        }
    }
});
*/

function enableChatInput(pre, title) {
    console.log("Chatbox input active");
    $("#chat-input").attr("disabled", false);
    $("#chat-input").focus();
    chatfocused = true;
    $("#chat-input").val("");
    $("#chat-label").animate({ opacity: 1 }, 100);
    if (pre.length > 0) {
        chatPrefix = pre + " ";
    }
    
    if (title == null) {
        title = title + ":";
    }
    $("#chat-label").html(title);
}

function disableChatInput() {
    console.log("Chatbox input hidden");
    $("#chat-input").attr("disabled", false);
    $("#chat-input").blur();
    chatfocused = false;
    $("#chat-input").val("");
    $("#chat-label").animate({ opacity: 0 }, 100);
    //$("#chat-label").html("Say:");
}

function pushChat(str) {
    $("#chatbody").append(str);
    //$("#chatbody").css("overflow", "auto");
    $("#chatbody").animate({ scrollTop: $("#chatbody")[0].scrollHeight }, "fast");
    //$("#chatbody").scrollTop($("#chatbody")[0].scrollHeight);
    $("#chatbody").css("overflow", "hidden");
}

function showChat(state) {
    if (state) {
        $("#chat-panel").show();
    } else {
        $("#chat-panel").hide();
    }
}