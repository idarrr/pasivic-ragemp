﻿using RAGE;
using System;
using System.Collections.Generic;
using System.Text;
using RAGE.Game;
using RAGE.Ui;

namespace NeverfallRoleplay
{
    class Chatbox : Events.Script
    {
        public static bool Active { get; set; }
        private static bool Showed = true;
        public Chatbox ()
        {
            Events.Add("OnChatEnter", OnChatEnter);
            //Pad.disable
            Chat.Show(false);
            Key.Bind(Key.KEY_T, false, SayChat);
            Key.Bind(Key.KEY_B, false, LocalOOCChat);
            Active = false;
            Events.Add("PushChat", (args) => { Push((string)args[0]); });
        }

        private static void LocalOOCChat()
        {
            if (!Showed)
            {
                return;
            }
            if (CefUI.IsInputActive())
            {
                return;
            }
            if (!Active)
            {
                Active = true;
                Cursor.Visible = true;
                CefUI.CefJSExecute("enableChatInput('/b', 'Local OOC');");
            }
        }
        
        private static void SayChat()
        {
            if (!Showed)
            {
                return;
            }
            if (CefUI.IsInputActive())
            {
                return;
            }
            if (!Active)
            {
                Active = true;
                Cursor.Visible = true;
                CefUI.CefJSExecute("enableChatInput('/t', 'Say');");
            }
        }

        private static void OnChatEnter(params object[] args)
        {
            if (args[0].ToString().Length < 1)
            {
                CefUI.CefJSExecute("disableChatInput();");
                Active = false;
                Cursor.Visible = false;
                return;
            }
            CefUI.CefJSExecute("disableChatInput();");
            Events.CallRemote("PlayerChat", args[0].ToString());
            Active = false;
            Cursor.Visible = false;
        }

        public static void Send(string text, int r, int g, int b)
        {
            Events.CallRemote("SendChat", text, new int[] { r, g, b });
        }

        public static void Send(string text)
        {
            Events.CallRemote("SendChat", text);
        }

        private static void Push (string s)
        {
            s = HTMLString.Escape(s);
            CefUI.CefJSExecute($"pushChat('{s}');");
        }

        public static void Show (bool state)
        {
            if (state)
            {
                CefUI.CefJSExecute($"showChat(true)");
            } else
            {
                CefUI.CefJSExecute($"showChat(false)");
            }
            Showed = state;
        }

        public static bool IsShowing()
        {
            return Showed;
        }
    }
}
