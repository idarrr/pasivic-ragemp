﻿using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CEF = RAGE.Ui.HtmlWindow;
using Newtonsoft.Json;

namespace NeverfallRoleplay
{
    class CefUI : Events.Script
    {
        public const string BASE_PATH = "package://cs_packages/NeverfallRoleplay/UI/html/";
        private static CEF browser = new CEF(BASE_PATH + "index.html");
        private static bool InputActive = false;

        public CefUI()
        {
            browser.Active = true;
            Events.Add("CefCallback", CefCallback);
            Events.Add("LoadWindow", LoadWindowE);
            Events.Add("CloseWindow", CloseWindow);
            Events.Add("CloseUI", CloseUI);
            Events.Add("OnWindowLoaded", OnWindowLoaded);
            Events.Add("ToggleUI", ToggleUI);
            Events.Add("OnInputFocusChange", OnInputFocusChange);
        }

        private void OnInputFocusChange(object[] args)
        {
            try
            {
                var state = (bool)args[0];
                InputActive = state;
            } catch (Exception e) {
                Logging.WriteException(e);
            }
        }

        public static bool IsInputActive()
        {
            
            return InputActive;
        }

        public static void ToggleUI(params object[] args)
        {
            browser.Active = !browser.Active;
        }
        public static void LoadUI(string path, string id, float x, float y, string w, string h, string query)
        {
            path = HTMLString.Escape(path);
            id = HTMLString.Escape(id);
            query = HTMLString.Escape(query);
            w = HTMLString.Escape(w);
            h = HTMLString.Escape(h);
            browser.ExecuteJs($"loadUI('{path}', '{id}', {x}, {y}, '{w}', '{h}', '{query}')");
        }

        public static void LoadWindow(string windowId, string title, float x, float y, float w, float h, string query)
        {
            browser.ExecuteJs($"loadWindow(\"{windowId}\", \"{title}\", {x}, {y}, {w}, {h}, \"{query}\");");
        }

        public static void LoadWindowE(params object[] args)
        {
            LoadWindow((string)args[0], (string)args[1], (float)args[2], (float)args[3], (float)args[4], (float)args[5], (string)args[6]);
        }

        public static void OnWindowLoaded(params object[] args)
        {
            var id = (string)args[0];
            //Chat.Output((string)args[0]);
            //CefExecute("example.html", "onReady", "Asdsd");
        }

        public static void CefCallback(params object[] args)
        {
            var eventName = (string)args[0];
            var newargs = new object[args.Length-1];
            Chat.Output(string.Join(", ", args));
            Array.Copy(args, 1, newargs, 0, args.Length - 1);
            
            Chat.Output(string.Join(", ", newargs));
            Events.CallRemote(eventName, newargs);
        }

        public static void CefExecute(string id, string function, params object[] args)
        {
            /* 0: ID
             * 1: Function Name
             * 3: Args
             */
            var length = args.Length + 2;
            var parameters = new object[length];
            parameters[0] = id;
            parameters[1] = function;
            var startfrom = 2;
            foreach (var item in args)
            {
                parameters[startfrom] = HTMLString.Escape(item);
                startfrom++;
            }
            
            var json = JsonConvert.SerializeObject(parameters);
            browser.ExecuteJs($"execute(...{json})");
        }

        public static void CefJSExecute(string str)
        {
            browser.ExecuteJs(str);
        }

        public static void CloseWindow(string id)
        {
            browser.ExecuteJs($"closeWindow(\"{id}\")");
        }

        public static void CloseUI(string id)
        {
            browser.ExecuteJs($"closeUI(\"{id}\")");
        }

        public static void CloseWindow(params object[] args)
        {
            CloseWindow((string)args[0]);
        }

        public static void CloseUI(params object[] args)
        {
            CloseUI((string)args[0]);
        }
    }
}
