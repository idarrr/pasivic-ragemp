﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class Bind
    {
        public int Key { get; }
        public int Key_2 { get; }
        public bool Pressed { get; set; }
        public bool State { get; set; }
        public Key.KeyBind Action;

        public bool DoubleKey { get; }

        public Bind ( int key, bool state, Key.KeyBind action)
        {
            Key = key;
            Action = action;
            Pressed = false;
            State = state;
            DoubleKey = false;
        }

        public Bind(int key, int key2, bool state, Key.KeyBind action)
        {
            Key = key;
            Key_2 = key2;
            Action = action;
            Pressed = false;
            State = state;
            DoubleKey = true;
        }

        public void CallAction()
        {
            if (!Chatbox.Active)
            {
                Action?.Invoke();
            }
        }
    }
}
