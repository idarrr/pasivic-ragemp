﻿using RAGE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeverfallRoleplay
{
    class Key : Events.Script
    {
        // Keys
        public const int KEY_0 = 0x30;
        public const int KEY_1 = 0x31;
        public const int KEY_2 = 0x32;
        public const int KEY_3 = 0x33;
        public const int KEY_4 = 0x34;
        public const int KEY_5 = 0x35;
        public const int KEY_6 = 0x36;
        public const int KEY_7 = 0x37;
        public const int KEY_8 = 0x38;
        public const int KEY_9 = 0x39;

        public const int KEY_F1 = 0x70;
        public const int KEY_F2 = 0x71;
        public const int KEY_F3 = 0x72;
        public const int KEY_F4 = 0x73;
        public const int KEY_F5 = 0x74;
        public const int KEY_F6 = 0x75;
        public const int KEY_F7 = 0x76;
        public const int KEY_F8 = 0x77;
        public const int KEY_F9 = 0x78;
        public const int KEY_F10 = 0x79;
        public const int KEY_F11 = 0x7A;
        public const int KEY_F12 = 0x7B;

        public const int KEY_A = 0x41;
        public const int KEY_B = 0x42;
        public const int KEY_C = 0x43;
        public const int KEY_D = 0x44;
        public const int KEY_E = 0x48;
        public const int KEY_F = 0x46;
        public const int KEY_G = 0x47;
        public const int KEY_H = 0x48;
        public const int KEY_I = 0x49;
        public const int KEY_J = 0x4A;
        public const int KEY_K = 0x4B;
        public const int KEY_L = 0x4C;
        public const int KEY_M = 0x4D;
        public const int KEY_N = 0x4E;
        public const int KEY_O = 0x4F;
        public const int KEY_P = 0x50;
        public const int KEY_Q = 0x51;
        public const int KEY_R = 0x52;
        public const int KEY_S = 0x53;
        public const int KEY_T = 0x54;
        public const int KEY_U = 0x55;
        public const int KEY_V = 0x56;
        public const int KEY_W = 0x57;
        public const int KEY_X = 0x58;
        public const int KEY_Y = 0x59;
        public const int KEY_Z = 0x5A;

        public const int MOUSE_LEFT = 0x01;
        public const int MOUSE_RIGHT = 0x02;
        public const int MOUSE_MIDDLE = 0x02;
        public const int BACKSPACE = 0x08;
        public const int TAB = 0x09;
        public const int ENTER = 0x0D;
        public const int CAPSLOCK = 0x14;
        public const int ESCAPE = 0x1B;
        public const int SPACEBAR = 0x20;
        public const int ARROW_LEFT = 0x1B;
        public const int ARROW_UP = 0x1B;
        public const int ARROW_RIGHT = 0x1B;
        public const int ARROW_DOWN = 0x1B;

        public const int NUMPAD_0 = 0x60;
        public const int NUMPAD_1 = 0x61;
        public const int NUMPAD_2 = 0x62;
        public const int NUMPAD_3 = 0x63;
        public const int NUMPAD_4 = 0x64;
        public const int NUMPAD_5 = 0x65;
        public const int NUMPAD_6 = 0x66;
        public const int NUMPAD_7 = 0x67;
        public const int NUMPAD_8 = 0x68;
        public const int NUMPAD_9 = 0x69;
        public const int NUMPAD_MULTIPLY = 0x6A;
        public const int NUMPAD_ADD = 0x6B;
        public const int NUMPAD_SEPARATOR = 0x6C;
        public const int NUMPAD_SUBSTRACT = 0x6D;
        public const int NUMPAD_DECIMAL = 0x6E;
        public const int NUMPAD_DIVIDE = 0x6F;

        public const int SHIFT_LEFT = 0xA0;
        public const int SHIFT_RIGHT = 0xA1;
        public const int CTRL_LEFT = 0x6F;
        public const int CTRL_RIGHT = 0x6F;

        public delegate void KeyBind();
        private static List<Bind> BINDS = new List<Bind>();

        public Key ()
        {
            Events.Tick += CheckKey;
        }

        private void CheckKey(List<Events.TickNametagData> nametags)
        {
            for (int i = 0; i < BINDS.Count; i++)
            {
                if (((BINDS[i].DoubleKey && Input.IsDown(BINDS[i].Key) && Input.IsDown(BINDS[i].Key_2)) || (!BINDS[i].DoubleKey && Input.IsDown(BINDS[i].Key))) && !Chatbox.Active)
                {
                    if (!BINDS[i].Pressed)
                    {
                        if (BINDS[i].State)
                        {
                            BINDS[i].CallAction();
                        }
                        BINDS[i].Pressed = true;
                    }
                }
                else if (Input.IsUp(BINDS[i].Key) && !Chatbox.Active)
                {
                    if (BINDS[i].Pressed)
                    {
                        if (!BINDS[i].State)
                        {
                            BINDS[i].CallAction();
                        }
                        BINDS[i].Pressed = false;
                    }
                }
            }        
        }

        public static void Bind(int key, bool state, KeyBind keyBind)
        {
            BINDS.Add(new Bind(key, state, keyBind));
        }

    }
}
