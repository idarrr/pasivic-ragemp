﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class AppearanceModel
    {
        public const int PROP_HATS = 0;
        public const int PROP_GLASSES = 1;
        public const int PROP_EARS = 2;
        public const int PROP_WATCHES = 6;
        public const int PROP_BRACELETS = 7;
        public const int CLOTH_MASKS = 1;
        public const int CLOTH_HAIRS = 2;
        public const int CLOTH_TORSOS = 3;
        public const int CLOTH_LEGS = 4;
        public const int CLOTH_BAGS = 5;
        public const int CLOTH_SHOES = 6;
        public const int CLOTH_ACCESSORIES = 7;
        public const int CLOTH_UNDERSHIRTS = 8;
        public const int CLOTH_ARMORS = 9;
        public const int CLOTH_DECALS = 10;
        public const int CLOTH_TOPS = 11;
        public const int NOTSET = -999;
        public int Gender { get; set; } = NOTSET;
        public int ShapeFirst { get; set; } = NOTSET;
        public int ShapeSecond { get; set; } = NOTSET;
        public float ShapeMix { get; set; }
        public int SkinFirst { get; set; } = NOTSET;
        public int SkinSecond { get; set; } = NOTSET;
        public float SkinMix { get; set; }

        public int EyeColor { get; set; } = NOTSET;
        public int HairColor { get; set; } = NOTSET;
        public int HairHighlightColor { get; set; } = NOTSET;
        public float[] FaceFeature { get; set; } = new float[20];
        public int[] HeadOverlay { get; set; } = new int[13];
        public int[] Cloth { get; set; } = new int[13];
        public int Hat { get; set; } = NOTSET;
        public int Glass { get; set; } = NOTSET;
        public int Ears { get; set; } = NOTSET;
        public int Watch { get; set; } = NOTSET;
        public int Bracelet { get; set; } = NOTSET;

        public AppearanceModel()
        {
            for (int i = 0; i < Cloth.Length; i++)
            {
                Cloth[i] = NOTSET;
            }
        }
    }
}
