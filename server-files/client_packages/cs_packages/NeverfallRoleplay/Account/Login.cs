﻿using Newtonsoft.Json;
using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Text;
using CEF = RAGE.Ui.HtmlWindow;

namespace NeverfallRoleplay
{
    class Login : Events.Script
    {
        private int camera;
        private Vector3 LoginCameraPos = new Vector3(0, 0, 250);
        private bool IsOnLoginScreen = false;
        private bool IsOnCharacterCreator = false;
        private Vector3 CreatorCameraPos = new Vector3(402.8664f, -997.5515f, -98.5f);
        private Vector3 CreatorCameraDir = new Vector3(402.8664f, -996.4108f, -98.5f);
        private Vector3 CreatorPlayerPos = new Vector3(402.8664f, -996.4108f, -99.00027f);
        private Ped DummyPed;

        private const string LOGIN_PANEL_ID = "login-panel";
        private const string CHARACTER_CREATOR_ID = "character-creator";
        private AppearanceModel appereance = new AppearanceModel();

        public readonly Login Instance;
        public Login ()
        {
            Events.Tick += UpdateCameraScene;
            Instance = this;
            //if (!IsLogged(Player.LocalPlayer))
            //{
                
                SetLoginScene();
                Chatbox.Show(false);
            //}
            Events.Add("OnLoginSend", OnLoginSend);
            Events.Add("OnRegisterSend", OnRegisterSend);
            Events.Add("RegisterAttempt", CallbackRegisterAttempt);
            Events.Add("LoginAttempt", CallbackLoginAttempt);
            Events.Add("OnLoginSuccess", OnLoginSuccess);
            Events.Add("OnAppereanceChange", OnAppereanceChange);
            Logging.WriteLine($"Dimension is {Player.LocalPlayer.Dimension}");
        }

        private void OnAppereanceChange(object[] args)
        {
            try
            {
                var c = 0;
                foreach (var item in args)
                {
                    Logging.WriteLine("Key" + c);
                    Logging.WriteLine(item);
                    c++;
                }
                var face1 = (int)args[0];
                var face2 = (int)args[1];
                var faceblend = Convert.ToSingle(args[2]);
                var skinblend = Convert.ToSingle(args[3]);
                var facefeature = JsonConvert.DeserializeObject<float[]>(args[4].ToString());
                var headblend = JsonConvert.DeserializeObject<int[]>(args[5].ToString());
                var clothes = JsonConvert.DeserializeObject<int[]>(args[6].ToString());
                var eyeColor = (int)args[7];
                var hairColor = (int)args[8];
                var hairHColor = (int)args[9];
                var gender = (int)args[10];

                appereance.Gender = gender;
                appereance.ShapeFirst = face1;
                appereance.ShapeSecond = face2;
                appereance.SkinFirst = face1;
                appereance.SkinSecond = face2;
                appereance.ShapeMix = faceblend;
                appereance.SkinMix = skinblend;
                appereance.EyeColor = eyeColor;
                appereance.HairColor = hairColor;
                appereance.HairHighlightColor = hairHColor;

                for (int i = 0; i < clothes.Length; i++)
                {
                    appereance.Cloth[i] = clothes[i];
                }

                for (int i = 0; i < headblend.Length; i++)
                {
                    appereance.HeadOverlay[i] = headblend[i];
                }

                for (int i = 0; i < facefeature.Length; i++)
                {
                    appereance.FaceFeature[i] = facefeature[i];
                }

                Events.CallRemote("OnCreationAppereanceUpdate", JsonConvert.SerializeObject(appereance));
            } catch (Exception e)
            {
                Logging.WriteException(e);
            }
        }

        private void OnGenderChange(object[] args)
        {

        }

        private void OnLoginSuccess(object[] args)
        {
            try
            {
                UnSetLoginScene();
                
                SetCharacterCreatorScene();
            } catch
            {

            }
        }

        private void CallbackLoginAttempt(object[] args)
        {
            try
            {
                var state = (bool)args[0];
                var msg = HTMLString.Escape(args[1]);
                if (state)
                {
                    CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { msg, "success", "LOGGIN IN" });
                    //browser.ExecuteJs($"callbackServer('{msg}', 'success', 'LOGGING IN')");
                }
                else
                {
                    CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { msg, "danger" });
                    //browser.ExecuteJs($"callbackServer('{msg}', 'danger')");
                }
                
            }
            catch
            {
                CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { "An error occurred. Try again.", "danger" });
            }
            
        }

        private void OnRegisterSend(object[] args)
        {
            try
            {
                Events.CallRemote("RegisterAttempt", args);
            }
            catch
            {
                CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { "An error occurred. Try again.", "danger" });
            }
        }

        private void CallbackRegisterAttempt(object[] args)
        {
            try
            {
                var state = (bool)args[0];
                var msg = HTMLString.Escape(args[1]);
                if (state)
                {
                    CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { msg, "success", "LOGGIN IN" });
                    //browser.ExecuteJs($"callbackServer('{msg}', 'success', 'LOGGING IN')");
                } else
                {
                    CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { msg, "danger" });
                    //browser.ExecuteJs($"callbackServer('{msg}', 'danger')");
                }
            } catch
            {
                CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { "An error occurred. Try again.", "danger" });
            }
        }

        private void OnLoginSend(object[] args)
        {
            try
            {
                Events.CallRemote("LoginAttempt", args);
            }
            catch
            {
                CefUI.CefExecute(LOGIN_PANEL_ID, "callbackServer", new object[] { "An error occurred. Try again.", "danger" });
            }
        }

        private void UpdateCameraScene(List<Events.TickNametagData> nametags)
        {
            if (IsOnLoginScreen || IsOnCharacterCreator)
            {
                //RAGE.Game.Ui.HideHudAndRadarThisFrame();
                LoginCameraPos = LoginCameraPos.Add(new Vector3(0.05f, 0, 0));
                RAGE.Game.Cam.SetCamCoord(camera, LoginCameraPos.X, LoginCameraPos.Y, LoginCameraPos.Z);
                if (IsOnCharacterCreator)
                {
                    var ppos = new Vector3(LoginCameraPos.X, LoginCameraPos.Y, LoginCameraPos.Z).Add(new Vector3(-3f, 0, 0.1f));
                    Player.LocalPlayer.Position = ppos;
                    Player.LocalPlayer.SetHeading(90f);
                }
            }
        }

        private void SetLoginScene()
        {
            IsOnLoginScreen = true;
            SetLoginCameraScene();
            CefUI.LoadUI("package://cs_packages/NeverfallRoleplay/UI/html/login.html", LOGIN_PANEL_ID, 0, 0, "100%", "100%", "");
        }

        private void UnSetLoginScene()
        {
            CefUI.CloseUI(LOGIN_PANEL_ID);
        }

        private void UnSetCameraScene()
        {
            RAGE.Game.Cam.SetCamActive(camera, false);
            RAGE.Game.Cam.RenderScriptCams(false, false, 0, true, false, camera);
        }

        private void SetLoginCameraScene()
        {
            camera = RAGE.Game.Cam.CreateCamWithParams("DEFAULT_SCRIPTED_FLY_CAMERA", LoginCameraPos.X, LoginCameraPos.Y, LoginCameraPos.Z, 0, 0, 90, 40, true, 2);
            RAGE.Game.Cam.SetCamActive(camera, true);
            RAGE.Game.Cam.RenderScriptCams(true, false, 0, true, false, camera);
        }

        private void SetCharacterCreatorScene()
        {
            IsOnCharacterCreator = true;
            //SetCreatorCameraScene();
            CefUI.LoadUI("package://cs_packages/NeverfallRoleplay/UI/html/character-creation.html", CHARACTER_CREATOR_ID, 0, 0, "100%", "100%", "");
            
        }

        private void UnSetCharacterCreatorScene()
        {
            CefUI.CloseUI(CHARACTER_CREATOR_ID);
        }

        private void SetCreatorCameraScene()
        {
            if (camera == 0)
            {
                camera = RAGE.Game.Cam.CreateCamWithParams("DEFAULT_SCRIPTED_FLY_CAMERA", CreatorCameraPos.X, CreatorCameraPos.Y, CreatorCameraPos.Z, 0, 0, 0, 40, true, 2);
            }
            RAGE.Game.Cam.SetCamCoord(camera, CreatorCameraPos.X, CreatorCameraPos.Y, CreatorCameraPos.Z);
            RAGE.Game.Cam.SetCamRot(camera, 0, 0, 0, 2);
            RAGE.Game.Cam.PointCamAtCoord(camera, CreatorCameraDir.X, CreatorCameraDir.Y, CreatorCameraDir.Z);
            RAGE.Game.Cam.SetCamActive(camera, true);
            if (!RAGE.Game.Cam.IsCamActive(camera))
            {
                RAGE.Game.Cam.RenderScriptCams(true, false, 0, true, false, camera);
            }
        }

        public static bool IsLogged(Player p)
        {
            try
            {
                int logged = (int)p.GetSharedData("logged");
                if (logged == 1)
                {
                    return true;
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return false;
        }
    }
}
