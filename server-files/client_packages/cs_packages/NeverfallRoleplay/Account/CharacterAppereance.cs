﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class CharacterAppereance
    {
        public int Face1 { get; set; }
        public int Face2 { get; set; }
        public float FaceBlend { get; set; }
        public float SkinBlend { get; set; }

        public float[] FaceFeature = new float[20];

        public int[] HeadOverlay = new int[13];

        public int[] ComponentVariations = new int[12];

        public CharacterAppereance()
        {
            for (int i = 0; i < 20; i++)
            {
                FaceFeature[i] = 0f;
            }
            for (int i = 0; i < 13; i++)
            {
                HeadOverlay[i] = 255;
            }
        }
    }
}
