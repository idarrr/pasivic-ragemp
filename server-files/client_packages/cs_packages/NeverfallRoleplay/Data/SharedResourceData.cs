﻿using Newtonsoft.Json;
using RAGE;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class SharedResourceData : Events.Script
    {
        private Dictionary<string, string> ResourceData = new Dictionary<string, string>();
        public SharedResourceData ()
        {
            Events.Add("InitialResourceData", OnInitialReceive);
            Events.Add("OnResourceDataChange", OnResourceDataChange);
        }

        private void OnResourceDataChange(object[] args)
        {
            try
            {
                var key = args[0].ToString();
                var val = args[1].ToString();
                ResourceData[key] = val;
            } catch { }
        }

        public string GetData(string key)
        {
            if (ResourceData.ContainsKey(key))
                return ResourceData[key];
            else
                return null;
        }

        private void OnInitialReceive(object[] args)
        {
            try
            {
                var s = args[0].ToString();
                var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);
                foreach (var item in data)
                {
                    ResourceData[item.Key] = item.Value;
                }
            } catch
            {

            }
        }
    }
}
