﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using RAGE;

namespace NeverfallRoleplay
{
    class Logging
    {
        // None of filesystem working except streamwriter (ragemp limitation)
        private static string DIR_LOG = @"pvrp.net/log";
        public static string LOGPATH = @"pvrpdotnet_script.log";
        public static string EXCEPTIONPATH = $@"pvrpdotnet_error.log";

        public static void WriteLine(object o)
        {
            using(var sw = new StreamWriter(LOGPATH, true))
            {
                sw.WriteLine(o.ToString());
            }
        }

        public static void WriteException(Exception e)
        {
            using (var sw = new StreamWriter(EXCEPTIONPATH, true))
            {
                sw.WriteLine($"ERROR: {e.Message}\n\rTRACE: {e.StackTrace}");
            }
        }
    }
}
