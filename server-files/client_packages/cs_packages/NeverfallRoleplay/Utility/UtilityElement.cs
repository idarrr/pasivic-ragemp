﻿using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeverfallRoleplay
{
    class UtilityElement
    {
        public static bool IsPlayer(Entity entity)
        {
            var id = entity.RemoteId;
            bool r = false;
            try
            {
                var player = Entities.Players.All.First(x => x.RemoteId.Equals(id));
                if (player != null)
                {
                    r = true;
                }
            } catch
            {

            }
            return r;
        }


    }
}
