﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class HTMLString
    {
        /* Dirty solution
         * Escaping single quote ' character to HTML Entities
         * Prevent JS not working due single quotes
         * more will added
         */

        public static string Escape(string s)
        {
            return s.Replace("'", "&#39;").Replace("\n", "&#13;").Replace("\\", "&#92;");
        }

        public static string Escape(object s)
        {
            return Escape(s.ToString());
        }
    }
}
