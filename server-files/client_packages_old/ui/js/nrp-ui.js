var draggedWindow;
$(document).ready(function () {
    $('.window').mousedown(function () {
        // set ohters element to the initial level
        $(this).siblings('.window').css('z-index', 10);
        // set clicked element to a higher level
        $(this).css('z-index', 11);
        //dragElement(this)
    });

    $('.window-header').mousedown(function (e) {
        //console.log(this);
        e = e || window.event;
        var window = $(this).parent('.window');
        window.data("pos1", 0);
        window.data("pos2", 0);
        window.data("pos3", e.clientX);
        window.data("pos4", e.clientY);
        //window.mousemove(onMouseMove);
        //window.bind("mousemove", onMouseMove);
        //console.log($(this).parent('.window').attr('id'));
        //console.log("attach");
        draggedWindow = window;
    });

    $('.window-header').mouseup(function () {
        var window = $(this).parent('.window');
        //if (window) {
        //window.off("mousemove");
        //window.unbind("mousemove", onMouseMove);
        //}
        //console.log("detach");
        //window.unbind("mousemove", onMouseMove);
        draggedWindow = null;
    });

    $(document).mousemove(function (e) {
        if (draggedWindow) {
            onMouseMove(e, draggedWindow);
        }
    });

    function onMouseMove(e, window) {
        //console.log(e);
        //var window = $(this);
        e = e || window.event;
        e.preventDefault();
        //var e = window.event;
        var pos1 = window.data("pos3") - e.clientX;
        var pos2 = window.data("pos4") - e.clientY;
        var pos3 = e.clientX;
        var pos4 = e.clientY;

        window.data("pos1", pos1);
        window.data("pos2", pos2);
        window.data("pos3", pos3);
        window.data("pos4", pos4);
        //console.log(window.offset().top, window.offset().left);
        //window.style.top = (window.offsetTop - pos2) + "px";
        //window.style.left = (window.offsetLeft - pos2) + "px";
        window.offset({ top: (window.offset().top - pos2), left: (window.offset().left - pos1) });
        //console.log(pos1, pos2);
    }

    $("#chat-input").change(function () {
        $(this).css("height: auto;").trigger("resize");

    });

    //$("#chat-input").autosize();
});