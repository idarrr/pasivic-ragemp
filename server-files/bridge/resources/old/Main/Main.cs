﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GTANetworkAPI;
using MySql.Data.MySqlClient;

namespace Main
{
    public class Main : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            var result = Database.FetchAll("SELECT * FROM `db`");
            for (int i = 0; i < result.Count; i++)
            {
                //Console.WriteLine("Row " + i);
                foreach (string key in result[i].Keys)
                {
                    //Console.WriteLine(key + ": " + result[i][key]);
                }
                //Console.WriteLine(" ");
            }
            TestC();
        }

        private async Task TestC ()
        {
            var result = await Database.FecthAllAsync("SELECT * FROM `db`");
            for (int i = 0; i < result.Count; i++)
            {
                Console.WriteLine("Row " + i);
                foreach (string key in result[i].Keys)
                {
                    Console.WriteLine(key + ": " + result[i][key]);
                }
                Console.WriteLine(" ");
            }
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client client)
        {
            
        }
    }
}
