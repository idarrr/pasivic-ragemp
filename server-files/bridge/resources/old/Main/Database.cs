﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using MySql.Data.MySqlClient;

namespace Main
{
    public class Database : Script
    {
        public static String TAG = "Database";
        private static readonly String Username = "root";
        private static readonly String Name = "mysql";
        private static readonly String Host = "localhost";
        private static readonly String Password = "";
        private static readonly String ConnectionString = string.Format($"Server={Host}; database={Name}; UID={Username}; password={Password}");
        public static MySqlConnection Connection = null;
        public static readonly long DATABASE_INSERT_FAILED = -1;
        
        [ServerEvent(Event.ResourceStart)]
        public static void OnResourceStart ()
        {
            Console.WriteLine(TAG + ": MySQL initialized...");
            Connection = new MySqlConnection(ConnectionString);
            Connection.Open();
            if (Connection.Ping())
            {
                Console.WriteLine(TAG + ": MySQL Connected! Version " + Connection.ServerVersion);
            } else
            {
                Console.WriteLine(TAG + ": Error connecting to MySQL Database!");
            }
        }

        public static MySqlConnection GetConnection()
        {
            return Connection;
        }
        
        public static List<Dictionary<string, object>> FetchAll (String query)
        {
            int start = Environment.TickCount;
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            var command = new MySqlCommand(query, Connection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    row[reader.GetName(i)] = reader.GetValue(i);
                }
                result.Add(row);
            }
            reader.Close();
            return result;
        }

        public static async Task<List<Dictionary<string, object>>> FecthAllAsync (String query)
        {
            int start = Environment.TickCount;
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            var command = new MySqlCommand(query, Connection);
            using(var reader = await command.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    Dictionary<string, object> row = new Dictionary<string, object>();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        row[reader.GetName(i)] = reader.GetValue(i);
                    }
                    result.Add(row);
                }
                reader.Close();
                Console.WriteLine("Execute time: " + (Environment.TickCount - start));
                return result;
            }
            //Console.WriteLine("Execute time: " + (Environment.TickCount - start));
            
        }

        public static Dictionary<string, object> FetchOne (String query)
        {
            Dictionary<string, object> result = null;
            var command = new MySqlCommand(query, Connection);
            var reader = command.ExecuteReader();
            if (reader.Read())
            {
                result = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    result[reader.GetName(i)] = reader.GetValue(i);
                }
            }
            reader.Close();
            return result;
        }

        public static void ExecuteQuery (params string[] Args)
        {

        }

        public static long Insert (string query)
        {
            MySqlCommand command = new MySqlCommand(query, Connection);
            int count = command.ExecuteNonQuery();
            if (count > 1)
            {
                return command.LastInsertedId;
            }
            return DATABASE_INSERT_FAILED;
        }

        public static string Escape (string str)
        {
            return MySql.Data.MySqlClient.MySqlHelper.EscapeString(str);
        }
    }
}
