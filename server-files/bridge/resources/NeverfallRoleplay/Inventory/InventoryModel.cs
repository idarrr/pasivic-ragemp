﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeverfallRoleplay.Inventory
{
    class InventoryModel
    {
        public const int TYPE_PLAYER = 1;
        public const int TYPE_VEHICLE = 2;
        public const int TYPE_OBJECT = 3;
        public const int TYPE_PED = 4;
        public const int TYPE_ITEM = 5;

        private int SizeX;
        private int SizeY;
        public float Capacity { get; private set; }
        private List<Item> Items = new List<Item>();
        public readonly int InventoryType;
        public readonly long OwnerId;

        public string Name { get; private set; } = "Inventory";
        private bool Loaded = false;
        public readonly long InventoryId;

        //Subscribers
        private List<Client> Subscribers;

        public InventoryModel(int ownerId, int inventoryType, int sizeX, int sizeY)
        {
            OwnerId = ownerId;
            InventoryType = inventoryType;
            SizeX = sizeX;
            SizeY = sizeY;
            InventoryId = InventoryManager.GetNextInventoryId();
        }

        // Core
        public async Task LoadItems()
        {
            if (Loaded)
            {
                return;
            }
            var items = await Database.FecthAllAsync("SELECT *");

            // TODO

            Loaded = true;
            NotifyAllSubscibers();
        }

        public List<Item> GetItems()
        {
            return Items;
        }

        public bool AddItem(Item item)
        {
            return true;
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            return true;
        }

        public bool RemoveItem(Item item)
        {
            return true;
        }

        public async Task<bool> RemoveItemAsync(Item item)
        {
            return true;
        }

        public bool MoveItem(Item item, int inventoryId)
        {
            return true;
        }

        public async Task<bool> MoveItemAsync(Item item, int inventoryId)
        {
            return true;
        }

        public bool DropItem(Item item, Vector3 position, int dimension)
        {
            return true;
        }

        public async Task<bool> DropItemAsync(Item item, Vector3 position, int dimension)
        {
            return true;
        }

        // Property method
        public int GetSizeX() { return SizeX; }
        public int GetSizeY() { return SizeY; }

        // Access control
        public bool HasAccess(Client player)
        {
            return true;
        }

        // Subscribers
        public void AddSubscriber(Client player)
        {
            if (Subscribers.Contains(player)) { return; }
            Subscribers.Add(player);
        }

        public void RemoveSubscriber(Client player)
        {
            if (!Subscribers.Contains(player)) { return; }
            Subscribers.Remove(player);
        }

        public void NotifySubsciber(Client player)
        {
            if (!Loaded) { return; }
        }

        public void NotifyAllSubscibers()
        {
            foreach (var player in Subscribers)
            {
                NotifySubsciber(player);
            }
        }
    }
}