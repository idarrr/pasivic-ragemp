﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Inventory
{
    public class ItemEntity
    {
        public int ItemIndex { get; private set; }
    }

    
    class InventoryManager
    {
        // Instance
        private static InventoryManager instance;
        public static InventoryManager GetInstance()
        {
            if (instance == null)
            {
                return instance = new InventoryManager();
            }
            return instance;
        }
        // Static fields
        private static long CurrentInventoryId = 0;
        // Static methods
        public static long GetNextInventoryId()
        {
            CurrentInventoryId += 1;
            return CurrentInventoryId;
        }

        public Dictionary<Client, EntityInventory<Client>> PlayersInventory = new Dictionary<Client, EntityInventory<Client>>();

        private InventoryManager()
        {
        }
    }
}
