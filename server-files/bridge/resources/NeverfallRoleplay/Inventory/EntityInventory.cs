﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Inventory
{
    /// <summary>
    /// <see cref="T"/> can be <see cref="GTANetworkAPI.Entity"/> or <see cref="ItemEntity"/>. ItemEntity virtually act as Entity that hold Item index inside of Inventory.
    /// See <see cref="InventoryModel"/> constant with TYPE keyword for list of Element Type
    /// </summary>
    class EntityInventory<T>
    {
        public T Entity { get; private set; }

        /// <summary>
        /// <see cref="EntityType"/> define what Entity corresponden for <see cref="EntityInventory{T}"/>.
        /// </summary>
        public int EntityType;
        public long EntityId;
        public List<InventoryModel> Inventories { get; private set; } = new List<InventoryModel>();

        /// <summary>
        /// <paramref name="entity"/> can be <see cref="GTANetworkAPI.Entity"/> or <see cref="ItemEntity"/>. <see cref="ItemEntity"/> hold information of ItemIndex from <see cref="Item"/>.
        /// <paramref name="entityId"/> is Element ID, if Element is <see cref="ItemEntity"/>, Item Index is used as identity
        /// </summary>
        public EntityInventory(T entity, long entityId)
        {
            if (entity is Client)
            {
                EntityType = InventoryModel.TYPE_PLAYER;
            }
            else if (entity is Ped)
            {
                EntityType = InventoryModel.TYPE_PED;
            }
            else if (entity is GTANetworkAPI.Object)
            {
                EntityType = InventoryModel.TYPE_OBJECT;
            }
            else if (entity is Vehicle)
            {
                EntityType = InventoryModel.TYPE_VEHICLE;
            }
            else if (entity is ItemEntity)
            {
                EntityType = InventoryModel.TYPE_ITEM;
            }
            EntityId = entityId;
            Entity = entity;
        }

        public void AddInventory(InventoryModel inventory)
        {
            Inventories.Add(inventory);
        }

        public void RemoveInventory(InventoryModel inventory)
        {
            Inventories.Remove(inventory);
        }
    }
}
