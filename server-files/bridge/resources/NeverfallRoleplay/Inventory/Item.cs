﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Inventory
{
    class Item
    {
        public const string NO_VALUE = "";

        public readonly long Index;
        public readonly ItemData Data;
        public int ItemId;
        public string ItemValue = NO_VALUE;
        public long Quantity;
        public int PositionX;
        public int PositionY;
        public int Durability;
        public long Created;

        public Item (long index, ItemData Data)
        {
            Index = index;
            this.Data = Data;
            
        }

        public long GetExpiration()
        {
            DateTime current = DateTime.Now;
            return 0;
        }

        public bool HasValue()
        {
            if (ItemValue.Equals(NO_VALUE))
            {
                return false;
            }
            return true;
        }
    }
}
