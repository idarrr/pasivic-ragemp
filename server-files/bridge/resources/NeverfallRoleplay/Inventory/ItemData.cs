﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Inventory
{
    class ItemData
    {
        public static long NO_EXPIRED = -1;
        public static string UNIT_KILO = "kg";
        public static string UNIT_GRAM = "g";
        public static string UNIT_ML = "mL";
        public static string UNIT_PCS = "Pcs";
        public static string UNIT_DOLLAR = "$";
        public const string UNIT_VOLUME = "m³";

        public int ItemId;
        public string ItemName;
        public string ItemDescription;
        public int MaxQuantity;
        public int SizeX;
        public int SizeY;
        public uint Model;
        public float Volume;

        public bool HasDurability = false;
        public long Expired = NO_EXPIRED;
        public string ExpiredMessage;
        public string DurabilityMessage;

        public bool CanExpired()
        {
            return Expired == NO_EXPIRED;
        }
    }
}
