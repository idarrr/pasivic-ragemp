﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Inventory
{
    class ItemManager
    {
        
        private ItemManager Instance;
        private Dictionary<int, ItemData> ItemDatas = new Dictionary<int, ItemData>();

        private ItemManager()
        {
            
        }

        public Dictionary<int, ItemData> GetItemDatas()
        {
            return ItemDatas;
        }

        public ItemData GetItemData(int ItemId)
        {
            return ItemDatas[ItemId];
        }

        public ItemManager GetInstance()
        {
            if (Instance == null)
            {
                Instance = new ItemManager();
            }
            return Instance;
        }
        /// <summary>
        /// Make player Use Item
        /// </summary>
        /// <param name="player">Client</param>
        /// <param name="inventory">Inventory where item exists</param>
        /// <param name="item">Item</param>
        /// <returns>boolean success, string reason</returns>
        public (bool, string) UseItem(Client player, InventoryModel inventory, Item item)
        {
            bool success = false;
            string reason = "Unknown Error";
            switch (item.ItemId)
            {
                case 1:
                    
                default:
                    break;
            }
            return (success, reason);
        }
    }
}
