﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Faction
{
    public class FactionPermissionModel
    {
        public const int NOT_ALLOWED = 0;
        public const int ALLOW_VIEW = 1;
        public const int ALLOW_MANAGE = 2;
        public int Id;
        public string Name;
        public List<long> AllowedVehicles = new List<long>();
        public List<long> AllowedInteriors = new List<long>();
        public List<long> AllowedObjects = new List<long>();

        public int PermissionFaction = NOT_ALLOWED;
        public int PermissionRank = NOT_ALLOWED;
        public int PermissionMember = NOT_ALLOWED;
        public int PermissionJob = NOT_ALLOWED;
        public int PermissionVehicle = NOT_ALLOWED;
        public int PermissionInterior = NOT_ALLOWED;
        public int PermissionObject = NOT_ALLOWED;
        public int PermissionRelation = NOT_ALLOWED;
        public int PermissionFinance = NOT_ALLOWED;

    }
}
