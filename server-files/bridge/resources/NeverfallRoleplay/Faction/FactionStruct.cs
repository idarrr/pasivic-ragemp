﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Faction
{
    public struct Member
    {
        public long CharId;
        public string CharName;
        public int Rank;
        public MemberJob Job;
        public bool Leader;
    }

    public struct RankCategory
    {
        public long Id;
        public string Name;
    }

    public struct Rank
    {
        public long Id;
        public long RankCategory;
        public string RankName;
        public int Position;
        public FactionPermissionModel Permission;
    }

    public struct FactionRelation
    {
        public const int RELATION_ALLY = 0;
        public const int RELATION_CHILD = 1;
        
        public int Relation;
        public long MainFaction;
        public long RelatedFaction;

    }

    public struct MemberJob
    {
        public const int SUNDAY = 0;
        public const int MONDAY = 1;
        public const int TUESDAY = 2;
        public const int WEDNESDAY = 3;
        public const int THURSDAY = 4;
        public const int FRIDAY = 5;
        public const int SATURDAY = 6;

        public long Id;
        public string Name;
        public long Salary;
        public Dictionary<int, bool> WorkingDay;
        public int StartHour;
        public int EndHour;

        /// <summary>
        /// Interior Id on <see cref="Interior.InteriorModel"/>.
        /// If not set, <see cref="InteriorWorkplace"/> are used for defining Working place
        /// </summary>
        public int InteriorWorkplace;
        /// <summary>
        /// Vector3 Position represent as working place
        /// </summary>
        public Vector3 PositionWorkplace;
        /// <summary>
        /// Dimension represent as working place
        /// </summary>
        public int DimensionWorkplace;
        /// <summary>
        /// Range of workplace by the <see cref="PositionWorkplace"/>
        /// </summary>
        public float RangeWorkplace;
    }

    public struct DutyPoint
    {
        public long Id;
        public Vector3 Position;
        public int Dimension;
        public string Name;
        public List<Rank> AllowedRanks;
    }
}
