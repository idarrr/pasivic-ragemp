﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Faction
{
    class FactionModel
    {
        
        const int TYPE_GOVERNMENT = 1;
        const int TYPE_LAW = 2;
        const int TYPE_MEDICAL = 3;
        const int TYPE_EDUCATION = 4;
        const int TYPE_MEDIA = 5;
        const int TYPE_BUSSINESS = 6;
        const int TYPE_ORGANIZATION = 7;
        const int TYPE_CORPORATION = 8;

        public long Id { get; private set; }
        public string Name { get; private set; }
        public int Type { get; private set; }

        public List<Member> Members { get; private set; } = new List<Member>();
        public List<RankCategory> Categories { get; private set; } = new List<RankCategory>();
        public List<Rank> Ranks { get; private set; } = new List<Rank>();



    }
}
