﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    
    class EntityData<T>
    {
        public delegate void EntityDataChange(string key, T olddata, T newdata);
        public static event EntityDataChange OnEntityDataChange;
        private static Dictionary<Entity, Dictionary<string, T>> Data = new Dictionary<Entity, Dictionary<string, T>>();
        
        public static void RegisterOnEntityDataChange(EntityDataChange change)
        {
            OnEntityDataChange += change;
        }
        

    }
}
