﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeverfallRoleplay
{
    
    class DataModel<T>
    {
        public delegate void OnEntityDataChange(int EntityID, string Key, T oldValue, T newValue);
        public event OnEntityDataChange onEntityDataChange;
        public readonly string Key;
        private T Value;
        private T OldValue;
        public readonly int EntityID;
        private List<Client> Subscriber = new List<Client>();

        public DataModel(string Key, T Value, int EntityID)
        {
            this.Key = Key;
            this.Value = Value;
            this.EntityID = EntityID;
        }

        public void RegisterEntityDataChange(OnEntityDataChange action)
        {
            onEntityDataChange += action;
        }

        public string GetKey() { return Key; }
        public T GetValue() { return Value; }
        public void SetValue(T Value) {
            OldValue = this.Value;
            this.Value = Value;
            UpdateChange();
        }

        private async void UpdateChange()
        {
            await Task.Run(() => {
                onEntityDataChange.Invoke(EntityID, Key, OldValue, Value);
                foreach (var item in Subscriber)
                {
                    item.TriggerEvent("OnEntityDataChange", EntityID, Key, OldValue, Value);
                }
            });
        }

        public List<Client> GetSubscribers()
        {
            return Subscriber;
        }

        public void AddSubscriber(Client client)
        {
            Subscriber.Add(client);
        }

        public void RemoveSubscriber(Client client)
        {
            Subscriber.Remove(client);
        }
    }
}
