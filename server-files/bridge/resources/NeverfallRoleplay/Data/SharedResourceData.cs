﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeverfallRoleplay
{
    public delegate void OnResourceDataReady();

    class ResourceDataModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool Sync { get; set; }
        public ResourceDataModel() { }
    }
    class SharedResourceData : Script
    {
        public static event OnResourceDataReady onResourceDataReady;
        private static Dictionary<string, ResourceDataModel> ResourceData = new Dictionary<string, ResourceDataModel>();
        private static string ResourceDataJSON = "";
        private static List<Client> Subs = new List<Client>();
        private static bool IsLoaded = false;
        public static void SetDefaultResourceData(string key, string value, bool sync)
        {
            if (ResourceData.ContainsKey(key))
                return;
            ResourceData[key] = new ResourceDataModel();
            ResourceData[key].Key = key;
            ResourceData[key].Value = value;
            ResourceData[key].Sync = sync;
            SaveResourceData(ResourceData[key]);
        }

        public static string GetResourceData(string key)
        {
            if (ResourceData.ContainsKey(key))
                return ResourceData[key].Value;
            else
                return null;
        }

        private static void SaveResourceData(ResourceDataModel data)
        {
            
        }

        public static void SetOnResourceDataReady(OnResourceDataReady action)
        {
            onResourceDataReady += action;
        }

        private static void SendToSubs(Client client, string key, string value)
        {
            client.TriggerEvent("OnResourceDataChange", key, value);
        }

        private static void SendToSubs(Client client)
        {
            client.TriggerEvent("InitialResourceData", ResourceDataJSON);
        }

        private static void GenerateJSON()
        {
            Dictionary<string, string> toSend = new Dictionary<string, string>();
            foreach (var item in ResourceData)
            {
                var data = item.Value;
                if (data.Sync)
                    toSend[data.Key] = data.Value;
            }
            ResourceDataJSON = JsonConvert.SerializeObject(toSend);
        }

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            SetOnResourceDataReady(OnReady);
            Database.SetOnDatabaseReady(() => { LoadAvailableDefaultData(); });
            
        }

        private void OnReady()
        {
            IsLoaded = true;
            foreach (var client in Subs)
            {
                client.TriggerEvent("InitialResourceData", ResourceDataJSON);
            }
        }

        [RemoteEvent("RegisterSubscriber")]
        public void RegisterSubscriber(Client client)
        {
            Subs.Add(client);
            if (IsLoaded)
                SendToSubs(client);
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void PlayerDisconnect(Client client, DisconnectionType type, string reason)
        {
            if (Subs.Contains(client))
                Subs.Remove(client);
        }

        public async void LoadAvailableDefaultData()
        {
            var result = await Database.FecthAllAsync("SELECT * FROM `resource_data`");
            if (result != null)
            {
                foreach (var item in result)
                {

                }
            }
            onResourceDataReady.Invoke();
        }

    }
}
