﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class Dimension
    {
        private static uint InitialDimension = uint.MaxValue-1;
        private static Dictionary<int, Client> Occuppied = new Dictionary<int, Client>();
        public int GlobalDimension { get { return (int)InitialDimension + 1; } }

        public static int AllocateLoginDimension(Client client)
        {
            int max = NAPI.Server.GetMaxPlayers();
            for (int i = 0; i < max; i++)
            {
                var dim = (int)InitialDimension-i;
                if (!Occuppied.ContainsKey(dim))
                {
                    Occuppied[dim] = client;
                    return dim;
                }
            }
            return 0;
        }

        public static void UnallocateLoginDimension(Client client)
        {
            foreach (var item in Occuppied)
            {
                if (item.Value.Equals(client))
                {
                    Occuppied.Remove(item.Key);
                }
            }
        }
    }
}
