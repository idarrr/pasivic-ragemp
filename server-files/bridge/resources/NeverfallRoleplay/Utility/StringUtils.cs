﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Utility
{
    class StringUtils
    {
        public static string Concatenate(string[] strings)
        {
            if (strings.Length == 0)
            {
                return "";
            } 
            StringBuilder builder = new StringBuilder();
            var first = true;
            foreach (string value in strings)
            {
                if (first)
                {
                    builder.Append(value);
                    first = false;
                } else
                {
                    builder.Append(' ');
                    builder.Append(value);
                }
                
                
            }
            return builder.ToString();
        }
    }
}
