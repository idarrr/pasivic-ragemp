﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class UI : Script
    {
        public static void Trigger(Client client, string windowId, string functionName, params object[] args)
        {
            var newargs = new object[] { windowId, functionName };
            for (int i = 0; i < args.Length; i++)
            {
                newargs[i + 2] = args[i];
            }
            client.TriggerEvent("CefExecute", newargs);
        }

        [Command("togui")]
        public static void ToggleUI(Client client)
        {
            client.TriggerEvent("ToggleUI");
        }
    }
}
