﻿using GTANetworkAPI;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NeverfallRoleplay
{
    class Account
    {
        public readonly string Username;
        public readonly long Id = -1;
        public readonly string Email;
        public readonly long Balance = 0;

        public Account (string username)
        {
            var info = Database.FetchOne($"SELECT * FROM `accounts` WHERE `username`='{Database.Escape(username)}'");
        }

        public Account (long Id)
        {
            var info = Database.FetchOne($"SELECT * FROM `accounts` WHERE `id`={Id}");
        }

        public Account (Client client)
        {
            var info = Database.FetchOne($"SELECT * FROM `accounts` WHERE `id`='{Database.Escape(client.GetData("id"))}'");
        }

        public bool IsValid()
        {
            if (Username != null && Id != -1)
            {
                return true;
            }
            return false;
        }

        public Client GetPlayer()
        {
            return Pool.GetPlayerByAccountId(Id);
        }

        public static string HashPassword(string password, string salt)
        {
            var provider = MD5.Create();
            byte[] bytes = provider.ComputeHash(Encoding.ASCII.GetBytes(salt + password));
            return BitConverter.ToString(bytes).ToUpper();
        }
    }
}
