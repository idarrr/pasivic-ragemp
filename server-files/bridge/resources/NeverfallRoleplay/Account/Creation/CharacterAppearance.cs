﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class CharacterAppearance : Script
    {
        private static CharacterAppearance instance = null;
        public CharacterAppearance()
        {
            instance = this;
        }

        [RemoteEvent("OnCreationAppereanceUpdate")]
        public void UpdateAppearance(Client player, string obj)
        {
            try
            {
                var app = JsonConvert.DeserializeObject<AppearanceModel>(obj);
                SetAppearance(player, app);
                
            } catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            
        }

        public void SetAppearance(Client player, AppearanceModel app)
        {
            if (app.Gender == 0)
            {
                if (!player.Model.Equals(NAPI.Util.GetHashKey("mp_m_freemode_01")))
                {
                    NAPI.Entity.SetEntityModel(player.Handle, NAPI.Util.GetHashKey("mp_m_freemode_01"));
                }
                
            } else if (app.Gender == 1)
            {
                if (!player.Model.Equals(NAPI.Util.GetHashKey("mp_f_freemode_01")))
                {
                    NAPI.Entity.SetEntityModel(player.Handle, NAPI.Util.GetHashKey("mp_f_freemode_01"));
                }
            }

            try
            {
                HeadBlend blend = app.GetHeadBlend();
                NAPI.Player.SetPlayerHeadBlend(player, blend);
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            for (int i = 0; i < app.FaceFeature.Length; i++)
            {
                NAPI.Player.SetPlayerFaceFeature(player, i, app.FaceFeature[i]);
            }

            if (app.EyeColor != AppearanceModel.NOTSET)
            {
                NAPI.Player.SetPlayerEyeColor(player, (byte)app.EyeColor);
            }

            if (app.HairColor != AppearanceModel.NOTSET && app.HairHighlightColor != AppearanceModel.NOTSET)
            {
                NAPI.Player.SetPlayerHairColor(player, (byte)app.HairColor, (byte)app.HairHighlightColor);
            }

            // Player Property will be synchronized to Client Side


            for (int i = 0; i < app.Cloth.Length; i++)
            {
                NAPI.Player.SetPlayerClothes(player, i, app.Cloth[i], 0);
            }
            
        }

        public static CharacterAppearance GetInstance()
        {
            return instance;
        }
    }
}
