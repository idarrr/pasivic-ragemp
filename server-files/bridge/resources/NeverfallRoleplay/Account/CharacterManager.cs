﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class CharacterManager : Script
    {
        private static CharacterManager _instance;
        private Dictionary<Client, Character> Characters = new Dictionary<Client, Character>();

        private CharacterManager()
        {
            _instance = this;
            Console.WriteLine("Character Manager started");
        }

        private void AddCharacter(Client client, Character character)
        {
            if (Characters.ContainsKey(client))
                return;
            Characters[client] = character;
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void PlayerDisconnect(Client client, DisconnectionType type, string reason)
        {
            if (!Characters.ContainsKey(client))
                return;
            Characters.Remove(client);
        }

        public static CharacterManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new CharacterManager();
            }
            return _instance;
        }
    }
}
