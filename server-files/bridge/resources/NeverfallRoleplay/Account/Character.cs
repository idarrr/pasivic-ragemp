﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class Character
    {
        private AppearanceModel Appearance;
        public readonly Client Player;
        private string FirstName;
        private string MiddleName;
        private string LastName;

        public Character(Client player)
        {
            
        }

        public string GetFullName(bool useMiddleName = false, bool returnFullMiddleName = true, bool useSpaceSeparator = false)
        {
            string separator = "_";
            if (useSpaceSeparator)
            {
                separator = " ";
            }
            if (!useMiddleName || MiddleName == null || MiddleName == "" || MiddleName.Length < 1)
            {
                return $"{FirstName}{separator}{LastName}";
            } else
            {
                if (returnFullMiddleName)
                {
                    return $"{FirstName}{separator}{MiddleName}{separator}{LastName}";
                } else
                {
                    return $"{FirstName}{separator}{MiddleName.Substring(0,1)}.{separator}{LastName}";
                }
            }
        }

        public string GetFullStaffName(bool useSpaceSeparator = false)
        {
            return Staff.GetStaffTitle(Player) + " " + GetFullName(true, false, useSpaceSeparator);
        }
    }
}
