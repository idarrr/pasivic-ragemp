﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using GTANetworkAPI;

namespace NeverfallRoleplay
{
    class Login : Script
    {
        private static readonly string TAG = "Login";
        

        public Login() { }

        [RemoteEvent("LoginAttempt")]
        public void LoginAttempt (Client player, string username, string password, bool automaticLogin)
        {
            var state = false;
            var msg = "An error occurred";
            try
            {
                var manager = AccountManager.Instance;
                if (manager.CanLogin(username, password))
                {
                    var info = manager.GetBannedState(username);
                    if (info.BannedRemaining != 0)
                    {
                        msg = $"Account '{username}' has been banned for '{info.BannedInDays} day(s)\nRemaining ban: {info.BannedRemaining} day(s).'";
                    } else
                    {
                        state = true;
                        msg = "Logging in...";
                        manager.DoLogin(player, username);
                    }
                } else
                {
                    msg = "Username or Password is incorrect or doesn't match.";
                }
                player.TriggerEvent("LoginAttempt", state, msg);
            } catch (Exception e)
            {
                Logging.PrintExeption(TAG, e);
                player.TriggerEvent("LoginAttempt", state, msg);
            }
        }

        [RemoteEvent("LoginAttemptAuto")]
        public void LoginAttemptAuto (Client player, string username, string hash)
        {
            var manager = AccountManager.Instance;
            if (manager.CanAutoLogin(username, hash))
            {

            }
        }

        [RemoteEvent("RegisterAttempt")]
        public void RegisterAttempt(Client player, string username, string email, string password)
        {
            var state = false;
            var message = "Error occurred";
            try
            {
                if (IsValidEmail(email))
                {
                    if (!DoesEmailExists(email))
                    {
                        if (username.Length >= 6 && username.Length <= 32)
                        {
                            if (IsValidUsernameFormat(username))
                            {
                                if (!DoesUsernameExists(username))
                                {
                                    if (password.Length >= 6 && password.Length <= 32)
                                    {
                                        var am = AccountManager.Instance;
                                        if (am.CreateAccount(email, username, password))
                                        {
                                            state = true;
                                            message = $"Your account <strong>{username}</strong> has been created. Logging in...";
                                            am.DoLogin(player, username);
                                        } else
                                        {
                                            message = $"Unable to create account for <strong>{username}</strong>. Try again or Contact Administrator.";
                                        }
                                    }
                                    else
                                    {
                                        message = $"Password must contain 6 up to 32 character.";
                                    }
                                }
                                else
                                {
                                    message = $"Username <strong>{username}</strong> already exists. Try different username.";
                                }
                            }
                        }
                        else
                        {
                            message = $"Username must contain 6 up to 32 character.";
                        }
                    } else
                    {
                        message = $"Email <strong>{email}</strong> already used. Try different email address.";
                    }
                } else
                {
                    message = $"Email '{email.ToString()}' is not a valid email address.";
                }
                player.TriggerEvent("RegisterAttempt", state, message);
            } catch (Exception e)
            {
                Console.WriteLine($"{TAG}: {e.Message}");
                Console.WriteLine($"{TAG}: {e.StackTrace}");
                player.TriggerEvent("RegisterAttempt", state, message);
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static bool DoesEmailExists(string email)
        {
            var row = Database.FetchSingleValue($"SELECT `id` FROM `accounts` WHERE LOWER(`email`) = '{Database.Escape(email).ToLower()}' ");
            return row != null;
        }

        public static bool DoesUsernameExists(string username)
        {
            var row = Database.FetchSingleValue($"SELECT `id` FROM `accounts` WHERE LOWER(`username`) = '{Database.Escape(username).ToLower()}' ");
            return row != null;
        }

        public static bool IsValidUsernameFormat(string username)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            return r.IsMatch(username);
        }

        private void OnPlayerLogin(Client player)
        {

        }

        [ServerEvent(Event.PlayerConnected)]
        private void OnPlayerConnected(Client player)
        {

        }
    }
}
