﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class BannedInfo
    {
        public string BannedReason { get; set; }
        public int BannedInDays { get; set; }
        public int BannedRemaining { get; set; }
        public string BannedBy { get; set; }
        public string BannedOn { get; set; }

        public BannedInfo()
        {
            BannedReason = "-";
            BannedBy = "-";
            BannedOn = "-";
        }

        public bool IsPermanent()
        {
            return BannedInDays == 999;
        }
    }
}
