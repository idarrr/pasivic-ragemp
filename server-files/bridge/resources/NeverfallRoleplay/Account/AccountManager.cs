﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NeverfallRoleplay
{
    class AccountManager
    {
        private static AccountManager _instance;
        private static int byteLength = 32;

        public static readonly string SHARED_DATA_USERNAME = "username";
        public static readonly string SHARED_DATA_LOGGED = "logged";
        public static readonly int LOGGED = 1;


        private AccountManager()
        {

        }

        public bool CanLogin(string username, string password)
        {
            var sU = Database.Escape(username);
            var result = Database.FetchOne($"SELECT `id`, `password`, `salt` FROM `accounts` WHERE `username`='{sU}'");
            if (result != null)
            {
                if (result.Count != 0)
                {
                    var pHash = result["password"].ToString();
                    var salt = result["salt"].ToString();
                    if (GenerateHash(SaltToByte(salt), password).Equals(pHash))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public BannedInfo GetBannedState(string username)
        {
            var info = new BannedInfo();
            try
            {
                var sU = Database.Escape(username);
                var result = Database.FetchOne($"SELECT `ban`, `ban_by`, `ban_date`, `ban_reason` FROM `accounts` WHERE `username`='{sU}'");
                if (result != null)
                {
                    info.BannedBy = result["ban_by"].ToString();
                    info.BannedInDays = Convert.ToInt32(result["ban"]);
                    info.BannedReason = result["ban_reason"].ToString();
                    info.BannedOn = result["ban_date"].ToString();
                }
            } catch
            {

            }
            return info;
        }

        public void DoLogin(Client player, string username)
        {
            player.SetSharedData(SHARED_DATA_USERNAME, username);
            player.SetSharedData(SHARED_DATA_LOGGED, LOGGED);
            Task.Run(() =>
            {
                NAPI.Task.Run(() => 
                { 
                    // in case client disconnected
                    try
                    {
                        player.TriggerEvent("OnLoginSuccess", username);
                    } catch { }
                }, delayTime: 2000);
            });
        }

        public bool CanAutoLogin(string username, string hash)
        {
            return false;
        }

        public static AccountManager Instance { 
            get {
                if (_instance == null)
                {
                    _instance = new AccountManager();
                }
                return _instance;
            } }

        public string GenerateHash(byte[] salt, string password)
        {
            using (var hash = new Rfc2898DeriveBytes(password, salt, 1000))
            {
                var bytes = hash.GetBytes(256);
                return Convert.ToBase64String(bytes);
            }  
        }

        public byte[] GenerateSalt()
        {
            var salt = new byte[byteLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }
            return salt;
        }

        public string SaltToString(byte[] salt)
        {
            return Convert.ToBase64String(salt);
        }

        public byte[] SaltToByte(string salt)
        {
            return Convert.FromBase64String(salt);
        }

        public bool CreateAccount(string email, string username, string password)
        {
            try
            {
                var isValid = false;
                byte[] salt = GenerateSalt();
                var saltstring = SaltToString(salt);
                var hash = GenerateHash(salt, password);
                var safeEmail = Database.Escape(email);
                var safeUsername = Database.Escape(username);
                var safePassword = Database.Escape(hash);
                var query = $"INSERT INTO `accounts` SET `email`='{safeEmail}', `username`='{safeUsername}', `password`='{safePassword}', `salt`='{saltstring}' ";
                Console.WriteLine(query);
                var insertid = Database.Insert(query);
                if (insertid != Database.DATABASE_INSERT_FAILED)
                {
                    isValid = true;
                }
                return isValid;
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
