﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NeverfallRoleplay.Web
{
    class HttpHandler : Script
    {
        public static string Domain = "localhost";
        public static string Port = "8080";

        private HttpListener listener;

        public HttpHandler ()
        {
            if (HttpListener.IsSupported)
            {
                Console.WriteLine("HttpListener is supported");
            } else
            {
                Console.WriteLine("HttpListener is not supported");
            }
            string pre = $"http://{Domain}:{Port}/papi/";
            listener = new HttpListener();
            listener.Prefixes.Add(pre);
            listener.Start();
            listener.BeginGetContext(OnContext, null);
        }

        private void OnContext(IAsyncResult res)
        {
            var ctx = listener.EndGetContext(res);
            var respon = ctx.Request;
            var query = respon.QueryString;
            var str = "";
            foreach (var item in query.Keys)
            {
                //str += item.ToString() + ": "+query.Get(item.ToString()) + "\n";
            }
            str = "<pre>"+ JsonConvert.SerializeObject(ClothingData.GetInstance(), Formatting.Indented) + "</pre>";
            listener.BeginGetContext(OnContext, null);
            Console.WriteLine(DateTime.UtcNow.ToString("HH:mm:ss.fff") + " Handling request");

            var buf = Encoding.ASCII.GetBytes(str);
            ctx.Response.ContentType = "text/html";

            // simulate work
            //Thread.Sleep(100);

            ctx.Response.OutputStream.Write(buf, 0, buf.Length);
            ctx.Response.OutputStream.Close();


            Console.WriteLine(DateTime.UtcNow.ToString("HH:mm:ss.fff") + " completed");
        }
    }
}
