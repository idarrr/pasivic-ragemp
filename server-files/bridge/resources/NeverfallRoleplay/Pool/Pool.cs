﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTANetworkAPI;

namespace NeverfallRoleplay
{
    class Pool : Script
    {

        [ServerEvent(Event.ResourceStart)]
        public static void OnResourceStart ()
        {
            
        }

        public static Client GetPlayerFromName (string name)
        {
            Client player = NAPI.Pools.GetAllPlayers().First(item => item.Name == name); 
            return player;
        }

        public static Client GetPlayerByUsername (string username)
        {
            Client player = NAPI.Pools.GetAllPlayers().First(item => item.GetData("username") == username);
            return player;
        }

        public static Client GetPlayerByAccountId (long Id)
        {
            Client player = NAPI.Pools.GetAllPlayers().First(item => item.GetData("id") == Id);
            return player;
        }

        public static Vehicle GetVehicleFromID (int Id)
        {
            Vehicle vehicle = NAPI.Pools.GetAllVehicles().First(item => item.GetData("id") == Id);
            return vehicle;
        }

        public static Vehicle GetVehicleFromData (string key, int value)
        {
            Vehicle vehicle = NAPI.Pools.GetAllVehicles().First(item => item.GetData(key) == value);
            return vehicle;
        }

        public static Vehicle GetVehicleFromData(string key, long value)
        {
            Vehicle vehicle = NAPI.Pools.GetAllVehicles().First(item => item.GetData(key) == value);
            return vehicle;
        }

        public static Vehicle GetVehicleFromData(string key, string value)
        {
            Vehicle vehicle = NAPI.Pools.GetAllVehicles().First(item => item.GetData(key) == value);
            return vehicle;
        }

    }
}
