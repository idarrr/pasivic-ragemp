﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    
    class ClothingData
    {
        public struct Model
        {
            public int Id;
            public string Name;
        }

        private static ClothingData instance;
        public static ClothingData GetInstance()
        {
            if (instance == null)
            {
                instance = new ClothingData();
            }
            return instance;
        }

        private const int MALE_HAIR_LENGTH = 75;
        private const int FEMALE_HAIR_LENGTH = 79;
        private const int MALE_TORSO_LENGTH = 167;
        private const int FEMALE_TORSO_LENGTH = 210;
        private const int MALE_LEGS_LENGTH = 127;
        private const int FEMALE_LEGS_LENGTH = 132;

        private const int MALE_SHOES_LENGTH = 96;
        private const int FEMALE_SHOES_LENGTH = 100;
        private const int MALE_TOPS_LENGTH = 332;
        private const int FEMALE_TOPS_LENGTH = 347;

        private const int HAIR_COLOR_LENGTH = 64;
        private const int EYE_COLOR_LENGTH = 32;

        public Dictionary<int, List<Model>> Hairs = new Dictionary<int, List<Model>>();
        public Dictionary<int, List<Model>> Torsos = new Dictionary<int, List<Model>>();
        public Dictionary<int, List<Model>> Legs = new Dictionary<int, List<Model>>();
        public Dictionary<int, List<Model>> Shoes = new Dictionary<int, List<Model>>();
        public Dictionary<int, List<Model>> Tops = new Dictionary<int, List<Model>>();
        public List<Model> HairColors = new List<Model>();
        public List<Model> EyeColors = new List<Model>();

        private ClothingData()
        {
            Hairs[AppearanceModel.GENDER_MALE] = new List<Model>();
            for (int i = 0; i < MALE_HAIR_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Male Hair " + i;
                Hairs[AppearanceModel.GENDER_MALE].Add(m);
            }
            Hairs[AppearanceModel.GENDER_FEMALE] = new List<Model>();
            for (int i = 0; i < FEMALE_HAIR_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Female Hair " + i;
                Hairs[AppearanceModel.GENDER_FEMALE].Add(m);
            }

            Torsos[AppearanceModel.GENDER_MALE] = new List<Model>();
            for (int i = 0; i < MALE_TORSO_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Male Torso " + i;
                Torsos[AppearanceModel.GENDER_MALE].Add(m);
            }
            Torsos[AppearanceModel.GENDER_FEMALE] = new List<Model>();
            for (int i = 0; i < FEMALE_TORSO_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Female Torso " + i;
                Torsos[AppearanceModel.GENDER_FEMALE].Add(m);
            }

            Legs[AppearanceModel.GENDER_MALE] = new List<Model>();
            for (int i = 0; i < MALE_LEGS_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Male Leg " + i;
                Legs[AppearanceModel.GENDER_MALE].Add(m);
            }
            Legs[AppearanceModel.GENDER_FEMALE] = new List<Model>();
            for (int i = 0; i < FEMALE_LEGS_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Female Leg " + i;
                Legs[AppearanceModel.GENDER_FEMALE].Add(m);
            }

            Shoes[AppearanceModel.GENDER_MALE] = new List<Model>();
            for (int i = 0; i < MALE_SHOES_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Male Shoes " + i;
                Shoes[AppearanceModel.GENDER_MALE].Add(m);
            }
            Shoes[AppearanceModel.GENDER_FEMALE] = new List<Model>();
            for (int i = 0; i < FEMALE_SHOES_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Female Shoes " + i;
                Shoes[AppearanceModel.GENDER_FEMALE].Add(m);
            }

            Tops[AppearanceModel.GENDER_MALE] = new List<Model>();
            for (int i = 0; i < MALE_TOPS_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Male Top " + i;
                Tops[AppearanceModel.GENDER_MALE].Add(m);
            }
            Tops[AppearanceModel.GENDER_FEMALE] = new List<Model>();
            for (int i = 0; i < FEMALE_TOPS_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Female Top " + i;
                Tops[AppearanceModel.GENDER_FEMALE].Add(m);
            }
            
            for (int i = 0; i < HAIR_COLOR_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Hair Color " + i;
                HairColors.Add(m);
            }
            for (int i = 0; i < EYE_COLOR_LENGTH; i++)
            {
                var m = new Model();
                m.Id = i;
                m.Name = "Hair Color " + i;
                EyeColors.Add(m);
            }
        }
    }
}
