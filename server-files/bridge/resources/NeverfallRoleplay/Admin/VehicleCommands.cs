﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class VehicleCommands
    {

        public VehicleCommands()
        {
            Commands.RegisterCommand("veh", SpawnTempVehicle);
        }

        public void SpawnTempVehicle(Client player, CommandInfo ci, params string[] args)
        {
            string VehicleName = Utility.StringUtils.Concatenate(args);
            Console.WriteLine($"'{VehicleName}'");
            uint VehicleHash = NAPI.Util.GetHashKey(VehicleName);
            Vehicle vehicle = NAPI.Vehicle.CreateVehicle(VehicleHash, player.Position.Around(3), 0, 0, 0);
            if (vehicle.IsNull)
            {
                Chat.Send(player, $"Invalid vehicle name for '{VehicleName}'", 255, 194, 14);
            } else
            {
                Chat.Send(player, $"Vehicle {vehicle.DisplayName} has been spawned", 255, 194, 14);
            }
            
        }
    }
}
