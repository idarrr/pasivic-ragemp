﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class CommandInfo
    {
        private const int CAN_NOT_USE = -1;
        public string CMD { get; }
        public string[] Syntax { get; }
        public string Description { get; }

        private int AdminAccess = CAN_NOT_USE;
        private int SupportAccess = CAN_NOT_USE;
        private int PlayerAccess = CAN_NOT_USE;
        private int ExecutiveAccess = 1;
        private int FactionType = CAN_NOT_USE;
        private int FactionID = CAN_NOT_USE;
        private Commands.PlayerCommand Action;

        public CommandInfo (string cmd, string[] syntax, string description, int adminAccess, int supportAccess, int executiveAccess, int playerAccess)
        {
            CMD = cmd;
            Syntax = syntax;
            AdminAccess = adminAccess;
            SupportAccess = supportAccess;
            ExecutiveAccess = executiveAccess;
            PlayerAccess = playerAccess;
            Description = description;
        }

        public bool CanUse(Client player)
        {
            if (Staff.IsExecutive(player) && ExecutiveAccess != CAN_NOT_USE)
            {
                if (Staff.GetExecutiveLevel(player) >= ExecutiveAccess)
                {
                    return true;
                }
            }
            if ((Staff.IsAdmin(player) && AdminAccess != CAN_NOT_USE) || Staff.IsExecutive(player))
            {
                if ((Staff.GetAdminLevel(player) >= AdminAccess) || Staff.IsExecutive(player))
                {
                    return true;
                }
            }
            if ((Staff.IsSupporter(player) && SupportAccess != CAN_NOT_USE) || Staff.IsExecutive(player))
            {
                if ((Staff.GetSupporterLevel(player) >= SupportAccess) || Staff.IsExecutive(player))
                {
                    return true;
                }
            }
            if (PlayerAccess != CAN_NOT_USE || Staff.IsExecutive(player))
            {
                return true;
            }
            return false;
        }

        public void SetAction(Commands.PlayerCommand action)
        {
            Action = action;
        }

        public void Execute(Client player, params string[] args)
        {
            if (Action != null)
            {
                Action.Invoke(player, this, args);
            } else
            {
                Console.WriteLine($"Action for command /{CMD} is not set.");
            }
            
        }
    }
}
