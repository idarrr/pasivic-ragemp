﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;

namespace NeverfallRoleplay
{
    class TeleportationCommands
    {
        public TeleportationCommands()
        {
            Commands.RegisterCommand("xyz", SetXYZ);
        }

        private void SetXYZ(Client player, CommandInfo cmd, string[] args)
        {
            try
            {
                var vect = new Vector3(float.Parse(args[0]), float.Parse(args[1]), float.Parse(args[2]));
                player.Position = vect;
            } catch
            {

            }
        }
    }
}
