﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class Staff
    {
        public static readonly string[] AdminLevels = new string[] {
            "Player",
            "Trial Administrator",
            "Administrator",
            "Senior Administrator",
            "Lead Administrator"
        };

        public static readonly string[] SupporterLevels = new string[] {
            "Player",
            "Trial Supporter",
            "Supporter",
            "Senior Supporter",
            "Lead Supporter"
        };

        public static readonly string[] ExecutiveLevels = new string[]
        {
            "Player",
            "Staff Executive",
            "Scripter",
            "Community Executive"
        };

        public static readonly int PLAYER = 0;
        public static readonly int TRIAL_ADMINISTRATOR = 1;
        public static readonly int ADMINISTRATOR = 2;
        public static readonly int SENIOR_ADMINISTRATOR = 3;
        public static readonly int LEAD_ADMINISTRATOR = 4;
        public static readonly int TRIAL_SUPPORTER = 1;
        public static readonly int SUPPORTER = 2;
        public static readonly int SENIOR_SUPPORTER = 3;
        public static readonly int LEAD_SUPPORTER = 4;
        public static readonly int STAFF_EXECUTIVE = 1;
        public static readonly int SCRIPTER = 2;
        public static readonly int COMMUNITY_EXECUTIVE = 3;

        public static int GetAdminLevel(Client client)
        {
            var admin = client.GetData("admin");
            if (admin is int)
            {
                return admin;
            }
            return 0;
        }

        public static int GetSupporterLevel(Client client)
        {
            var admin = client.GetData("supporter");
            if (admin is int)
            {
                return admin;
            }
            return 0;
        }

        public static int GetExecutiveLevel(Client client)
        {
            var exe = client.GetData("executive");
            if (exe is int)
            {
                return exe;
            }
            return 0;
        }

        public static bool IsStaff(Client client)
        {
            var admin = GetAdminLevel(client);
            var supporter = GetSupporterLevel(client);
            var exe = GetExecutiveLevel(client);
            if (admin > 0)
            {
                return true;
            }
            
            if (supporter > 0)
            {
                return true;
            }
            if (exe > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsAdmin(Client client)
        {
            var admin = GetAdminLevel(client);
            if (admin > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsHiddenStaff(Client client)
        {
            var hidden = client.GetData("hiddenadmin");
            if (hidden is int && hidden == 1)
            {
                return true;
            }
            return false;
        }

        public static bool IsExecutive(Client client)
        {
            var admin = GetAdminLevel(client);
            if (admin > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsAdmin(Client client, int level)
        {
            var admin = GetAdminLevel(client);
            if (admin > level)
            {
                return true;
            }
            return false;
        }

        public static bool IsSupporter(Client client)
        {
            var supporter = GetSupporterLevel(client);
            if (supporter > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsSupporter(Client client, int level)
        {
            var supporter = GetSupporterLevel(client);
            if (supporter > level)
            {
                return true;
            }
            return false;
        }

        public static string GetStaffTitle(Client client)
        {
            string title = "Player";
            if (IsAdmin(client))
            {
                return AdminLevels[GetAdminLevel(client)];
            }
            if (IsSupporter(client))
            {
                return SupporterLevels[GetSupporterLevel(client)];
            }
            if (IsExecutive(client))
            {
                return SupporterLevels[GetExecutiveLevel(client)];
            }
            return title;
        }

        public static string GetFullStaffName(Client client)
        {
            if (IsHiddenStaff(client))
            {
                return "Hidden Staff";
            }
            return GetStaffTitle(client) + " " + client.Name.Replace("_", " ");
        }
    }
}
