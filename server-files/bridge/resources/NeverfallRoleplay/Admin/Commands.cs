﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace NeverfallRoleplay
{
    class Commands : Script
    {
        public delegate void PlayerCommand(Client player, CommandInfo cmd, params string[] args);
        private static List<CommandInfo> COMMANDS = new List<CommandInfo>();
        private static bool Loaded = false;
        private static Dictionary<string, PlayerCommand> QueueRegister = new Dictionary<string, PlayerCommand>();
        private static Dictionary<string, PlayerCommand> UnregisteredCommands = new Dictionary<string, PlayerCommand>();
        private static readonly string TAG = "Commands";

        private void AdminSpawnVehicle(Client player, string VehicleName)
        {
            uint VehicleHash = NAPI.Util.GetHashKey(VehicleName);
            Vehicle vehicle = NAPI.Vehicle.CreateVehicle(VehicleHash, player.Position.Around(3), 0, 0, 0);
            vehicle.Neons = true;
            player.SendChatMessage($"Vehicle {vehicle.DisplayName} has been spawned.");
            
        }

        public void Cmds (Client player, CommandInfo commandInfo, params string[] args)
        {
            Chat.Send(player, "Command fired!", 0, 255, 0);
        }

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            RegisterCommand("cmds", Cmds);

            new VehicleCommands();
            new TeleportationCommands();
            Database.SetOnDatabaseReady(() => {
                //var am = AccountManager.Instance;
                //var can = Login.DoesUsernameExists("idarrr");
                //Console.WriteLine($"Exists: {can}");
                LoadAllCommands();
            });
            
        }

        private async void LoadAllCommands()
        {
            Console.WriteLine("Loading all commands from Database...");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var cmds = await Database.FecthAllAsync("SELECT * FROM `commands`");
            if (cmds == null)
            {
                Console.WriteLine($"{TAG}: Error. Can not load Commands.");
                return;
            }
            foreach (var item in cmds)
            {
                var cmd = await Task.Run(() =>
                {
                    var cmdstr = (string)item["command"];
                    var desc = (string)item["description"];
                    var syntax = (string)item["syntax"];
                    var syntaxArr = JsonConvert.DeserializeObject<string[]>(syntax);
                    var aAccess = (int)item["admin"];
                    var sAccess = (int)item["supporter"];
                    var eAccess = (int)item["executive"];
                    var pAccess = (int)item["player"];
                    CommandInfo c = new CommandInfo(cmdstr, syntaxArr, desc, aAccess, sAccess, eAccess, pAccess);
                    return c;
                });
                COMMANDS.Add(cmd);
            }
            stopwatch.Stop();
            Console.WriteLine($"All commands have been loaded, took {stopwatch.Elapsed.Milliseconds} miliseconds.");
            
            LoadAllQueuedCommands();
        }

        private async void LoadAllQueuedCommands()
        {
            Console.WriteLine("Loading all commands queued from scripts...");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int rC = 0;
            int uC = 0;
            foreach (var item in QueueRegister)
            {
                var cmd = await Task.Run(() =>
                {
                    CommandInfo r = null;
                    try
                    {
                        r = COMMANDS.First(x => x.CMD.Equals(item.Key));
                    } catch
                    {

                    }
                    return r;
                });

                if (cmd != null)
                {
                    rC += 1;
                    cmd.SetAction(item.Value);
                } else
                {
                    uC += 1;
                    UnregisteredCommands[item.Key] = item.Value;
                }
            }
            stopwatch.Stop();
            Console.WriteLine($"All Queued Commands have been assigned into Registered Commands, took {stopwatch.Elapsed.Milliseconds} miliseconds.");
            Console.WriteLine($"Registered Command added: {rC}");
            Console.WriteLine($"Un-Registered Command: {uC}");
        }

        public static void TriggerCommand(Client player, string command, string[] args)
        {
            try
            {
                var cmd = COMMANDS.First(item => item.CMD.Equals(command));
                if (cmd.CanUse(player))
                {
                    cmd.Execute(player, args);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Command {command} not found. {e.Message}");
            }
        }

        public static void RegisterCommand(string command, PlayerCommand pCmd)
        {
            if (!Loaded)
            {
                QueueRegister[command] = pCmd;
            } else
            {
                var cmd = COMMANDS.First(item => item.CMD.Equals(pCmd));
                if (cmd == null)
                {
                    Console.WriteLine($"Command /{command} is not registered and added to Unregistered Command Dictionary");
                    UnregisteredCommands[command] = pCmd;
                } else
                {
                    cmd.SetAction(pCmd);
                }
            }
        }
    }
}
