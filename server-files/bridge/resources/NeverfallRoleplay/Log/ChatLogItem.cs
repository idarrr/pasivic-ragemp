﻿using GTANetworkAPI;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class ChatLogItem
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Character { get; set; }
        public string Account { get; set; }
        public string Text { get; set; }
        public Color Color = new Color(255, 255, 255, 255);
        public DateTime Time = DateTime.Now;

    }
}
