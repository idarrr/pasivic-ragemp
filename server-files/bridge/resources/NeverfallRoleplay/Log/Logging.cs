﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;
using MongoDB;

namespace NeverfallRoleplay
{
    class Logging
    {
        public const string PATH = "/script.log";
        private const string TAG = "Logging";
        public static Logging Instance;

        // MongoDB
        public const string LOG_DATABASE = "logs";
        public const string CHATLOG_COLLECTIONS = "chatlogs";
        public const string COMMANDS_COLLECTIONS = "cmds";
        public const string CONNECT_COLLECTIONS = "connects";

        public Logging()
        {
            Instance = this;
        }
        public static void WriteLine(object o)
        {
            //System.IO.File.WriteAllText(PATH, o.ToString());
        }

        public static void PrintExeption(string TAG, Exception e)
        {
            Console.WriteLine("");
            Console.WriteLine($"{TAG}: {e.Message}");
            Console.WriteLine($"{TAG}: {e.StackTrace}");
            Console.WriteLine("");
        }

        public static async void RecordChatLog(Client client, string text, Color color)
        {
            try
            {
                var doc = new ChatLogItem()
                {
                    Color = color,
                    Text = text,
                    Character = client.Name,
                    Account = client.GetSharedData(AccountManager.SHARED_DATA_USERNAME)
                };
                MongoDB.Driver.MongoClient c = new MongoDB.Driver.MongoClient();
                var db = c.GetDatabase(LOG_DATABASE);
                var col = db.GetCollection<ChatLogItem>(CHATLOG_COLLECTIONS);
                await col.InsertOneAsync(doc);
            } catch (Exception e)
            {
                PrintExeption(TAG, e);
            }
        }
    }
}
