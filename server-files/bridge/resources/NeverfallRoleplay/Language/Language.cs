﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay
{
    class Language
    {
        public static readonly string[] LANGUAGES = new string[] {
            "English",
            "French",
            "Arabic",
            "Spanish",
            "Portuguese",
            "Hausa",
            "German",
            "Serbo-Croatian",
            "Italian",
            "Malay",
            "Russian",
            "Swahili",
            "Yoruba",
            "Albanian",
            "Dutch",
            "Hindustani",
            "Persian",
            "Quechua",
            "Tamil",
            "Sotho",
            "Chinese",
            "Aymara",
            "Bengali",
            "Berber",
            "Greek",
            "Guarani",
            "Hausa",
            "Korean",
            "Romanian",
            "Rwanda-Rundi",
            "Sotho",
            "Swati",
            "Swedish",
            "Tswana",
            "Turkish"
        };

        public static int GetPlayerLanguage(Client client)
        {
            var lang = client.GetData("language");
            if (lang is int)
            {
                return lang;
            }
            return 0;
        }

        public static String GetPlayerLanguageName(Client client)
        {
            return LANGUAGES[GetPlayerLanguage(client)];
        }
    }
}
