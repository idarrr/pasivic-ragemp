﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace NeverfallRoleplay
{
    class Chat : Script
    {
        public const int LOCAL_IC_CHAT_DISTANCE_MAX = 15;
        public const int LOCAL_OOC_CHAT_DISTANCE_MAX = 20;

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            Commands.RegisterCommand("t", (p, c, s) => {
                LocalICChat(p, String.Join(" ", s));
            });
            Commands.RegisterCommand("b", (p, c, s) => {
                LocalOOCChat(p, String.Join(" ", s));
            });
        }

        public static string GetName(Client client)
        {
            var maskid = client.GetData("maskid");
            if (maskid is int)
            {
                return "Unkown.Person." + maskid;
            }
            return client.Name.Replace("_", " ");
        }

        //[ServerEvent(Event.ChatMessage)]
        public static void ICChat(Client player, string text)
        {
            var nearby = NAPI.Pools.GetAllPlayers().FindAll(NearbyPlayer(player, 15));
            var inCar = player.Vehicle is Vehicle;
            foreach (var target in nearby)
            {
                if (inCar)
                {
                    if (target.Vehicle.Equals(player.Vehicle))
                    {
                        target.SendChatMessage($" [{Language.GetPlayerLanguageName(player)}] [In-Vehicle] {GetName(player)} says: {text}");
                    }
                } else
                {
                    target.SendChatMessage($" [{Language.GetPlayerLanguageName(player)}] {GetName(player)} says: {text}");
                }
            }
        }
        static Predicate<Client> NearbyPlayer(Client player, int range)
        {
            return delegate (Client target)
            {
                return player.Position.DistanceTo(target.Position) < range;
            };
        }

        [RemoteEvent("PlayerChat")]
        public void PlayerChat(Client player, params object[] args)
        {
            var text = args[0].ToString();

            if (text.StartsWith("/"))
            {
                var arr = text.Split(" ");
                var arg = text.Replace(arr[0]+" ", "").Split(" ");
                Commands.TriggerCommand(player, arr[0].Replace("/", ""), arg);
            }
        }

        [RemoteEvent("SendChat")]
        public void SendChat(Client player, params object[] args)
        {
            var text = args[0].ToString();
            int r = 255;
            int g = 255;
            int b = 255;
            if (args.Length > 1)
            {
                int[] color = (int[])args[2];
                r = color[0];
                g = color[1];
                b = color[2];
            }
            Send(player, text, r, g, b);
        }

        public static void Send(Client player, string text, int r, int g, int b)
        {
            text = WebUtility.HtmlEncode(text);
            var cT = "<div style=\"color: rgb(" + r + "," + g + "," + b + ",255)\" class=\"chat-item\">"
                    + text
                    + "</div>";
            //cT = cT.Replace("\\", @"\\").Replace("'", "\\'");
            player.TriggerEvent("PushChat", cT);
        }

        public static void LocalICChat(Client player, string text)
        {
            if (text.Length < 1)
            {
                return;
            }

            var lang = Language.GetPlayerLanguageName(player);

            foreach (var target in NAPI.Pools.GetAllPlayers())
            {
                if (target.Position.DistanceTo(target.Position) <= LOCAL_IC_CHAT_DISTANCE_MAX)
                {
                    Send(target, $" [{lang}] {player.Name.Replace("_", " ")} says: {text}", 255, 255, 255);
                }
            }
        }

        public static void LocalOOCChat(Client player, string text)
        {
            if (text.Length < 1)
            {
                return;
            }

            foreach (var target in NAPI.Pools.GetAllPlayers())
            {
                if (target.Position.DistanceTo(target.Position) <= LOCAL_IC_CHAT_DISTANCE_MAX)
                {
                    Send(target, $" {player.Name.Replace("_", " ")}: (({text}))", 138, 183, 255);
                }
            }
        }
    }
}
