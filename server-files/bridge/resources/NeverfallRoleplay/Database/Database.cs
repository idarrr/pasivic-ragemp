﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
using MySql.Data.MySqlClient;

namespace NeverfallRoleplay
{
    public delegate void DatabaseReady();
    public class Database : Script
    {
        static event DatabaseReady OnDatabaseReady;
        public static String TAG = "Database";
        private static readonly String Username = "root";
        private static readonly String DatabaseName = "ragemp";
        private static readonly String Host = "localhost";
        private static readonly String Password = "";
        private static readonly String ConnectionString = string.Format($"Server={Host}; database={DatabaseName}; UID={Username}; password={Password}");
        public static MySqlConnection Connection = null;
        public static readonly long DATABASE_INSERT_FAILED = -1;

        [ServerEvent(Event.ResourceStart)]
        public static void OnResourceStart()
        {
            Console.WriteLine(TAG + ": MySQL initialized...");
            Connection = new MySqlConnection(ConnectionString);
            Connection.Open();
            if (Connection.Ping())
            {
                OnDatabaseReady();
                Console.WriteLine(TAG + ": MySQL Connected! Version " + Connection.ServerVersion);
            }
            else
            {
                Console.WriteLine(TAG + ": Error connecting to MySQL Database!");
            }
        }

        public static void SetOnDatabaseReady(DatabaseReady databaseReady)
        {
            if (Connection != null && Connection.Ping())
            {
                databaseReady.Invoke();
            } else
            {
                OnDatabaseReady += databaseReady;
            }
        }

        public static MySqlConnection GetConnection()
        {
            return Connection;
        }

        public static List<Dictionary<string, object>> FetchAll(String query)
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            using (var command = new MySqlCommand(query, Connection))
            {
                using(var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Dictionary<string, object> row = new Dictionary<string, object>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            row[reader.GetName(i)] = reader.GetValue(i);
                        }
                        result.Add(row);
                    }
                    return result;
                }
            }
        }

        public static async Task<List<Dictionary<string, object>>> FecthAllAsync(String query)
        {
            try
            {
                using (var Connection = new MySqlConnection(ConnectionString))
                {
                    Connection.Open();
                    int start = Environment.TickCount;
                    List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
                    using (var command = new MySqlCommand(query, Connection))
                    {
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                Dictionary<string, object> row = new Dictionary<string, object>();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    row[reader.GetName(i)] = reader.GetValue(i);
                                }
                                result.Add(row);
                            }
                            Connection.Close();
                            Console.WriteLine("Execute time: " + (Environment.TickCount - start));
                            return result;
                        }
                    }
                }
            } catch (Exception e)
            {
                Console.WriteLine($"{TAG}: {e.Message}");
                Console.WriteLine($"{TAG}: {e.StackTrace}");
            }
            return null;
        }

        public static Dictionary<string, object> FetchOne(String query)
        {
            Dictionary<string, object> result;
            using (var command = new MySqlCommand(query, Connection)) {
                using(var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        result = new Dictionary<string, object>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            result[reader.GetName(i)] = reader.GetValue(i);
                        }
                        return result;
                    }
                }
            }
            return null;
        }

        /*
         * Return a Single Value from Query
         * Ex: SELECT `id` FROM `account` WHERE `username` = bla
         */
        public static object FetchSingleValue(String query)
        {
            object result = null;
            using(var command = new MySqlCommand(query, Connection))
            {
                using(var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        result = reader.GetValue(0);
                    }
                    return result;
                }
            }
        }

        public static void ExecuteQuery(params string[] Args)
        {

        }

        public static long Insert(string query)
        {
            using(var command = new MySqlCommand(query, Connection))
            {
                var count = command.ExecuteNonQuery();
                if (count > 0)
                {
                    var id = command.LastInsertedId;
                    return id;
                }
            }
            return DATABASE_INSERT_FAILED;
        }

        public static string Escape(string str)
        {
            return MySqlHelper.EscapeString(str);
        }
    }
}
