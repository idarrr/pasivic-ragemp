CREATE TABLE IF NOT EXISTS `accounts` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`username` VARCHAR(255),
	`password` VARCHAR(1024),
	`salt` VARCHAR(255),
	`email` VARCHAR(255),
	`discord` VARCHAR(255),
	`serial` VARCHAR(255),
	`ip` VARCHAR(255),
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`lastlogin` TIMESTAMP, 

	`loginhash` VARCHAR(1024),
	`lastcharacter` INT NOT NULL DEFAULT 0,
	`cash` INT NOT NULL DEFAULT 0,
	
	`ban` INT(10) NOT NULL DEFAULT 0,
	`ban_date` TIMESTAMP,
	`ban_reason` TEXT,
	`ban_by` VARCHAR(1024),

	`jail` INT(10) NOT NULL DEFAULT 0,
	`jail_date` TIMESTAMP,
	`jail_served` INT (10) NOT NULL DEFAULT 0,
	`jail_character` VARCHAR(255),
	`jail_reason` TEXT,
	`jail_by` VARCHAR(1024),

	`admin` INT NOT NULL DEFAULT 0,
	`support` INT NOT NULL DEFAULT 0,
	`executive` INT NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS `characters` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`account` INT NOT NULL DEFAULT 0,
	`firstname` VARCHAR(255),
	`middlename` VARCHAR(255),
	`lastname` VARCHAR(255),
	`dateofbirth` TIMESTAMP,
	`dna` VARCHAR(255),
	`gender` INT NOT NULL DEFAULT 0,
	`appearance` TEXT,
	
	
);

CREATE TABLE IF NOT EXISTS `items` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`itemid` INT
);