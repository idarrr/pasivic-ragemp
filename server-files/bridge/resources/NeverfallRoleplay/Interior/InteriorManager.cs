﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeverfallRoleplay.Interior
{
    class InteriorManager
    {
        private static InteriorManager instance;

        private Dictionary<Client, InteriorModel> OnColShape = new Dictionary<Client, InteriorModel>();

        public static InteriorManager GetInstance()
        {
            if (instance == null)
            {
                instance = new InteriorManager();
            }
            return instance;
        }

        public void OnEnterColShape(Client player, InteriorModel interior)
        {
            OnColShape[player] = interior;
        }

        public void OnExitColShape(Client player)
        {
            OnColShape.Remove(player);
        }

        public List<InteriorModel> Interiors { get; private set; } = new List<InteriorModel>();

        public InteriorManager ()
        {
            _ = LoadAllInterior();
        }

        private async Task LoadAllInterior()
        {

        }

        public InteriorModel GetInteriorById(long id)
        {
            InteriorModel interior = null;
            foreach (var item in Interiors)
            {
                if (item.Id.Equals(id))
                {
                    interior = item;
                    break;
                }
            }
            return interior;
        }

        public List<InteriorModel> GetInteriorOwnedByPlayer(long CharId)
        {
            List<InteriorModel> interiors = new List<InteriorModel>();
            Parallel.ForEach(Interiors, interior =>
               {
                   if (interior.Owner.Equals(CharId))
                   {
                       interiors.Add(interior);
                   }
               }
            );
            return interiors;
        }

    }
}
