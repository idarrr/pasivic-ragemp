﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeverfallRoleplay.Interior
{
    class InteriorModel
    {
        const int NO_OWNER = -1;

        const int TYPE_DEFAULT = 0;
        const int TYPE_HOUSE = 1;
        const int TYPE_ROOM = 2;
        const int TYPE_HOTEL = 3;
        const int TYPE_GARAGE = 4;
        const int TYPE_WORKSHOP = 5;
        const int TYPE_WAREHOUSE = 6;
        const int TYPE_OFFICE = 7;
        const int TYPE_SHOP = 8;
               
        const int CATEGORY_PRIVATE = 1;
        const int CATEGORY_COMMERCIAL = 2;

        const float COLSHAPE_RANGE = 2f;
        const float COLSHAPE_HEIGHT = 2f;

        public long Id { get; private set; }
        public String Name { get; private set; }
        public uint DimensionInside { get; private set; }
        public uint DimensionOutside { get; private set; }
        public Vector3 Outside { get; private set; }
        public float OutsideAngle { get; private set; }
        public Vector3 Inside { get; private set; }
        public float InsideAngle { get; private set; }
        public int Type { get; private set; }
        public int Category { get; private set; }
        public int Price { get; private set; }

        public long Owner { get; private set; } = -1;
        public long Faction { get; private set; } = -1;


        // Status
        public int Locked { get; set; }

        // Entity
        public ColShape ColShapeInside { get; private set; }
        public ColShape ColShapeOutside { get; private set; }

        private List<Client> OnColShapeInside = new List<Client>();
        private List<Client> OnColShapeOutside = new List<Client>();

        private InteriorManager manager;

        public InteriorModel(InteriorManager manager, long id)
        {
            this.manager = manager;
        }

        public void SpawnColShape()
        {
            if (ColShapeInside != null)
            {
                NAPI.Entity.DeleteEntity(ColShapeInside.Handle);
            }
            ColShapeInside = NAPI.ColShape.CreateCylinderColShape(Inside, COLSHAPE_RANGE, COLSHAPE_HEIGHT, (uint)DimensionInside);
            if (ColShapeOutside != null)
            {
                NAPI.Entity.DeleteEntity(ColShapeOutside.Handle);
            }
            ColShapeOutside = NAPI.ColShape.CreateCylinderColShape(Outside, COLSHAPE_RANGE, COLSHAPE_HEIGHT, (uint)DimensionOutside);
            ColShapeOutside.OnEntityEnterColShape += OnEntityEnterOutsideColShape;
            ColShapeOutside.OnEntityExitColShape += OnEntityExitOutsideColShape;
            ColShapeInside.OnEntityEnterColShape += OnEntityEnterInsideColShape;
            ColShapeInside.OnEntityExitColShape += OnEntityExitInsideColShape;

            
        }

        private void OnEntityExitInsideColShape(ColShape colShape, Client client)
        {
            OnColShapeInside.Remove(client);
            manager.OnExitColShape(client);
        }

        private void OnEntityExitOutsideColShape(ColShape colShape, Client client)
        {
            OnColShapeOutside.Remove(client);
            manager.OnExitColShape(client);
        }

        private void OnEntityEnterInsideColShape(ColShape colShape, Client client)
        {
            OnColShapeInside.Add(client);
            manager.OnEnterColShape(client, this);
        }

        private void OnEntityEnterOutsideColShape(ColShape colShape, Client client)
        {
            OnColShapeOutside.Add(client);
            manager.OnEnterColShape(client, this);
        }

        public void Teleport(Client player)
        {
            player.Freeze(true);
            if (OnColShapeOutside.Contains(player))
            {
                // Teleport to inside
                player.Position = Inside;
                player.Dimension = DimensionInside;
            } else if (OnColShapeInside.Contains(player))
            {
                // Teleport to outside
                player.Position = Outside;
                player.Dimension = DimensionOutside;
            }
            player.Freeze(false);
        }

        public bool IsBelongsToFaction()
        {
            return Faction != NO_OWNER;
        }
    }
}
